<?php
/**
 * The template for displaying 404 pages (Not Found).
 * * @package WordPress
 * @subpackage community-all-stars
 * @since community-all-stars 1.0
 */

get_header(); ?>
<div class="hero-section">
    <div class="w-container hero hero-home">
      <img class="w-hidden-tiny football-player" src="<?php bloginfo("template_directory"); ?>/images/player.png" alt="Football Player" data-ix="player-appearing">
      <header class="w-row">
        <div class="w-col w-col-7">
          <h1 class="w-clearfix hero-call" data-ix="text-appearing">Error 404</strong></h1>
          <p class="hero-paragraph" data-ix="text-appearing-2">We couldn't find the requested page.</p>
        </div>
        <div class="w-col w-col-5"></div>
      </header>
    </div>
  </div>
<?php get_footer(); ?>