<?php

/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after.  Calls sidebar-footer.php for bottom widgets.
 * * @package WordPress
 * @subpackage community-all-stars
 * @since community-all-stars 1.0
 */

?>
  <div class="home-footer">
    <div class="w-container">
      <div class="w-row">
        <div class="w-col w-col-2">
          <div class="footer-section">
            <h3 class="home-footer-title">Get around:</h3>
            <?php wp_nav_menu( array( 'theme_location' => 'footer_menu', 'menu_class' => 'w-list-unstyled footer-menu footer-menu-home', 'container' => false) ); ?>
          </div>
        </div>
        <div class="w-col w-col-4">
          <div class="footer-section">
            <h3 class="home-footer-title">About:</h3>
            <p class="home-footer-paragraph">Community All-Stars is completely free to high schools nationwide and aims to help promote, sustain, brand and fund high school athletics. We are directly contracted by high schools across the country.</p>
          </div>
        </div>
        <div class="w-col w-col-3">
          <div class="footer-section">
            <h3 class="home-footer-title">Contact:</h3>
            <p class="home-footer-paragraph"><strong>Questions?</strong> 866-558-1047
              <br>For a full list of departments and contact info please visit our contact us page.</p><a href="<?php echo get_permalink_by_name('contact');?>">Contact Us</a>
          </div>
        </div>
        <div class="w-col w-col-3">
          <div class="w-clearfix footer-section-last">
            <div class="w-hidden-tiny w-clearfix social-links social-links-footer-home">
              <a href="https://www.facebook.com/pages/Community-All-Stars/151063808300640" target="_blank"><img class="social-link home-social-link" src="<?php bloginfo("template_directory"); ?>/images/home-social-face.png" width="19" alt="Facebook"></a>
              <a href="https://www.facebook.com/pages/Community-All-Stars/151063808300640" target="_blank"><img class="social-link home-social-link" src="<?php bloginfo("template_directory"); ?>/images/home-social-twitter.png" width="19" alt="Twitter"></a>
              <a href="https://www.facebook.com/pages/Community-All-Stars/151063808300640" target="_blank"><img class="social-link home-social-link" src="<?php bloginfo("template_directory"); ?>/images/home-social-google.png" alt="Google+"></a>
              <a class="school-resources-link" href="<?php echo get_permalink_by_name('schools');?>">School Resources</a>
            </div>
            <img class="home-hat" src="<?php bloginfo("template_directory"); ?>/images/home-hat.jpg">
            <a href="http://www.bbb.org/san-diego/business-reviews/fund-raising-counselors-and-orgs/community-all-stars-llc-in-san-diego-ca-171997569/" target="_blank"><img class="left" src="<?php bloginfo("template_directory"); ?>/images/bbb.png"></a>
            <p class="proud-to-be-left">Proud to be accredited with an “A” rating by the Better Business Bureau.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="copyright home-copyright section">
    <div class="w-container">
      <a href="<?php echo home_url( '/' ); ?>">
        <img class="copyright-child logo" src="<?php bloginfo("template_directory"); ?>/images/home-copy-logo.png" width="42">
      </a>
      <p class="copyright-child"><small class="logo-title">Community All-Stars</small> &nbsp;&nbsp;&nbsp;&nbsp;&copy;<?php echo get_the_date('Y');?> Community All Stars LLC</p>
    </div>
  </div>
  <!--[if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif]-->
  <script src="<?php bloginfo("template_directory"); ?>/js/jquery.cycle.min.js"></script>
<?php wp_footer(); ?>
</body>
</html>