<?php

/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after.  Calls sidebar-footer.php for bottom widgets.
 * * @package WordPress
 * @subpackage community-all-stars
 * @since community-all-stars 1.0
 */

?>
  <div class="footer section">
    <div class="w-container">
      <div class="w-row">
        <div class="w-col w-col-9 w-col-stack">
          <div class="w-row footer-row">
            <div class="w-col w-col-4 w-col-small-4">
              <a class="w-inline-block logo footer-link" href="<?php echo home_url( '/' ); ?>">
                <img class="w-hidden-small" src="<?php bloginfo("template_directory"); ?>/images/cas_logo.png" width="54" alt="Logo">
                <h6 class="inline">Community All-Stars</h6>
              </a>
            </div>
            <div class="w-col w-col-4 w-col-small-4">
              <h6>© 2014 Community All-Stars LLC</h6>
            </div>
            <div class="w-col w-col-4 w-col-small-4">
              <h6><strong>Questions?</strong> 866-558-1047</h6>
            </div>
          </div>
          <div class="w-row footer-row">
            <div class="w-col w-col-4 w-col-small-4">
              <h4 class="subheading">quick contact:</h4>
              <p class="footer-paragraph"><strong>Ph: 866-558-1047</strong>
                <br>
                <br>Sponsorships: Extension 1.
                <br>Art and Design: Extension 2.
                <br>School Relations: Extension 3.
                <br>Community Relations: Extension 4. Accounts: Extension 5.</p>
            </div>
            <div class="w-col w-col-4 w-col-small-4">
              <?php wp_nav_menu( array( 'theme_location' => 'footer_menu', 'menu_class' => 'w-list-unstyled footer-menu', 'container' => false) ); ?>
            </div>
            <div class="w-col w-col-4 w-col-small-4">
              <div>Connect with us on social media:</div>
              <a href="https://www.facebook.com/pages/Community-All-Stars/151063808300640" target="_blank"><img class="social-link social-link-footer" src="<?php bloginfo("template_directory"); ?>/images/facebook-social-footer.png"></a>
              <a href="https://www.facebook.com/pages/Community-All-Stars/151063808300640" target="_blank"><img class="social-link social-link-footer" src="<?php bloginfo("template_directory"); ?>/images/twitter-social-footer.png"></a>
              <a href="https://www.facebook.com/pages/Community-All-Stars/151063808300640" target="_blank"><img class="social-link social-link-footer" src="<?php bloginfo("template_directory"); ?>/images/google-social-footer.png"></a>
            </div>
          </div>
          <div class="w-row">
            <div class="w-col w-col-4">
              <a href="http://www.bbb.org/san-diego/business-reviews/fund-raising-counselors-and-orgs/community-all-stars-llc-in-san-diego-ca-171997569/" target="_blank"><img class="left" src="<?php bloginfo("template_directory"); ?>/images/bbb.png"></a>
              <p class="footer-paragraph">Proud to be accredited with an “A” rating by the Better Business Bureau.
              </p>
            </div>
            <div class="w-col w-col-4">
              <img src="<?php bloginfo("template_directory"); ?>/images/authorize.png">
            </div>
            <div class="w-col w-col-4"></div>
          </div>
        </div>
        <div class="w-col w-col-3 w-col-stack">
          <div class="w-embed w-iframe w-hidden-medium w-hidden-small w-hidden-tiny facebook-widget">
            <iframe src="https://www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fpages%2FCommunity-All-Stars%2F151063808300640&amp;width=220&amp;height=290&amp;colorscheme=light&amp;show_faces=true&amp;header=true&amp;stream=true&amp;show_border=false&amp;appId=435779819838961" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:220px; height:290px;" allowtransparency="true"></iframe>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="copyright section">
    <div class="w-container">
      <a href="<?php echo home_url( '/' ); ?>">
        <img class="copyright-child" src="<?php bloginfo("template_directory"); ?>/images/cas_logo.png" width="42">
      </a>
      <div class="copyright-child">Community All-Stars</div>
      <div class="copyright-child">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Leader in Scholastic Athletic Branding and Funding.</div>
    </div>
  </div>
  <!--[if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif]-->
<?php wp_footer(); ?>
</body>
</html>