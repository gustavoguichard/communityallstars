<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
 
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<title><?php
		/*
		 * Print the <title> tag based on what is being viewed.
		 */
		global $page, $paged;

		wp_title( '|', true, 'right' );

		// Add the blog name.
		bloginfo( 'name' );

		// Add the blog description for the home/front page.
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) )
			echo " | $site_description";

		// Add a page number if necessary:
		if ( $paged >= 2 || $page >= 2 )
			echo ' | ' . sprintf( __( 'Page %s', '' ), max( $paged, $page ) );

		?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
  <script>
    WebFont.load({
      google: {
        families: ["Open Sans Condensed:300,300italic,700"]
      }
    });
  </script>
	<script type="text/javascript" src="<?php bloginfo("template_directory"); ?>/js/modernizr.js"></script>
  <link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo("template_directory"); ?>/images/favicon.ico">
  <link rel="apple-touch-icon" href="<?php bloginfo("template_directory"); ?>/images/webclip.png">
	<?php	wp_head();?>
</head>
<?php if(is_home()):?>
<body <?php body_class("home-page"); ?>>
<?php else:?>
<body <?php body_class(); ?>>
<?php endif;?>

	<nav class="main-menu <?php if(is_home()){echo "home-main-menu";}?>">
    <div class="w-container">
      <div class="w-row">
        <div class="w-col w-col-3">
          <a class="w-inline-block red-section-link logo" href="<?php echo home_url( '/' ); ?>">
            <img src="<?php bloginfo("template_directory"); ?>/images/cas_logo.png" width="63" alt="Logo">
            <h1 class="logo-header">Community All-Stars</h1>
          </a>
        </div>
        <div class="w-col w-col-9">
        	<?php if(!is_home()):?>
        	<div class="w-hidden-tiny w-clearfix social-links">
        		<a class="red-section-link school-resources-link" href="<?php echo get_permalink_by_name('schools');?>">School Resources</a>
            <a href="https://www.facebook.com/pages/Community-All-Stars/151063808300640" target="_blank"><img class="social-link" src="<?php bloginfo("template_directory"); ?>/images/google-social.png" width="19" alt="Google Plus"></a>
            <a href="https://www.facebook.com/pages/Community-All-Stars/151063808300640" target="_blank"><img class="social-link" src="<?php bloginfo("template_directory"); ?>/images/twitter-social.png" width="19" alt="Twitter"></a>
            <a href="https://www.facebook.com/pages/Community-All-Stars/151063808300640" target="_blank"><img class="social-link" src="<?php bloginfo("template_directory"); ?>/images/facebook-social.png" width="19" alt="Facebook"></a>
          </div>
          <?php else:?>
            <p class="questions_phone"><strong>Questions? </strong>866-558-1047</p>
					<?php endif;?>
          <div class="menu-options">
          	<?php wp_nav_menu( array( 'theme_location' => 'nav_menu', 'container' => false, 'items_wrap' => '%3$s') ); ?>
            <div class="w-form search-form-wrapper">
              <form class="search-form" id="wf-form-search-form" name="wf-form-search-form" role="search" method="get" action="<?php echo home_url( '/' ); ?>">
								<input class="w-input input" type="text" id="s" name="s" placeholder="School Name" />
								<input class="w-button submit-search" type="submit" value="Find" />
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </nav>