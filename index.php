<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 * * @package WordPress
 * @subpackage community-all-stars
 * @since community-all-stars 1.0
 */

get_header(); ?>
  <div class="section home-main-section">
    <div class="w-container">
      <div class="w-row home-main">
        <div class="w-col w-col-7 full-width-column">
          <div class="home-call-section">
            <h1 class="home-main-title" data-ix="text-appearing">Designed Exclusively<br>
<strong>for High School Athletics.</strong></h1>
            <div data-ix="text-appearing"><strong>100% free</strong> to high schools nation-wide.</div>
            <blockquote class="testimonial testimonial-home" data-ix="text-appearing-2">
              <p>“I would recommend <strong>every high school in the country use Community All-Stars</strong>.”</p>
              <div class="testimonial-author home-author">Kevin Biggs, Assistant Principal of Athletics, Mission Viejo High School</div>
              <a class="body-link" href="#video" rel="leanModal">Watch Video</a>
            </blockquote>
            <div class="item-home-learn-more">
              <img class="inline" src="<?php bloginfo("template_directory"); ?>/images/home-circle-red.png">
              <div class="inline">For <strong>Athletic Directors, Coaches and Schools:</strong>
              </div><a class="body-link inline" href="<?php echo get_permalink_by_name('schools');?>">Learn more</a>
            </div>
            <div class="item-home-learn-more">
              <img class="inline" src="<?php bloginfo("template_directory"); ?>/images/home-circle-blue.png">
              <div class="inline">For&nbsp;<strong>Sponsors and Businesses:</strong>
              </div><a class="body-link inline" href="<?php echo get_permalink_by_name('sponsors');?>">Learn more</a>
            </div>
            <div class="home-search-section" data-ix="text-appearing-2">
              <div class="w-form">
                <form class="home-form" id="wf-form-search-form" name="wf-form-search-form" role="search" method="get" action="<?php echo home_url( '/' ); ?>">
                  <h5 class="home-form-title">To <strong>Find a School or Team:</strong></h5>
                  <input class="w-input input home-input" id="s" type="text" placeholder="School Name" name="s">
                  <input class="w-button submit-search home-submit" type="submit" value="Find">
                </form>
              </div>
            </div>
          </div>
        </div>
        <div id="playerSlider" class="w-col w-col-5 full-width-column home-colorfull-column">
          <img class="w-hidden-small w-hidden-tiny home-player" src="<?php bloginfo("template_directory"); ?>/images/home-player.png" alt="Player" data-ix="player-appearing">
          <img class="w-hidden-small w-hidden-tiny home-player home-player-2" src="<?php bloginfo("template_directory"); ?>/images/soccer-player-no-ball.png" alt="Player" data-ix="player-appearing">
          <img class="w-hidden-small w-hidden-tiny home-player home-player-3" src="<?php bloginfo("template_directory"); ?>/images/basketball-player.png" alt="Player" data-ix="player-appearing">
        </div>
      </div>
    </div>
  </div>
  <div class="section home-sponsors-section">
    <div class="w-container">
      <div class="w-col w-col-9 full-width-column">
        <h2>
          Coaches, Athletics Directors &amp; Boosters:<br/>
          <strong>Brand &amp; Fund Your Teams like the Pros. For free.</strong>
        </h2>
      </div>
      <div class="w-col w-col-3 home-proud-to-be full-width-column">
        <a href="http://www.bbb.org/san-diego/business-reviews/fund-raising-counselors-and-orgs/community-all-stars-llc-in-san-diego-ca-171997569/" target="_blank"><img class="left" src="<?php bloginfo("template_directory"); ?>/images/bbb.png"></a>
        <p class="proud-to-be-left">Proud to be accredited with an “A” rating by the <strong>Better Business Bureau</strong>.</p>
      </div>
    </div>
  </div>
  <div class="home-featured-section">
    <div class="w-container">
      <div class="w-row">
        <div class="w-col w-col-12 home-magazines-section">
          <img class="w-hidden-small w-hidden-tiny" src="<?php bloginfo("template_directory"); ?>/images/home-magazines.png" alt="Magazines">
        </div>
      </div>
      <div class="w-row home-featured-row">
        <div class="w-col w-col-8 full-width-column">
          <div class="home-featured-column">
            <h3 class="home-featured-title">Welcome to the team.</h3>
            <p class="home-featured-paragraph">
              The leaders in <strong>scholastic athletic marketing, branding and funding</strong>. 
              We are a full service branding and funding agency designed exclusively for High School Athletics.
            </p>
            <a class="button home-button" href="<?php echo get_permalink_by_name('schools');?>">Schools</a>
            <a class="button home-button" href="<?php echo get_permalink_by_name('sponsors');?>">Sponsors</a>
          </div>
        </div>
        <div class="w-col w-col-4 full-width-column">
          <div class="get-visible">
            <blockquote class="red-testimonial featured-site-testimonial">
              <p>A community based approach to <strong>sustaining high school athletics and promoting local business</strong>.</p>
            </blockquote>
            <h3 class="get-visible-title">Sponsors: Get Visible.</h3>
            <p class="get-visible-paragraph">
              Associate your business with a great cause and get valuable visibility by <strong>sponsoring your local high school</strong>.
            </p>
            <p class="get-visible-paragraph"><a href="<?php echo get_permalink_by_name('sponsors');?>">Learn More</a></p>
          </div>
        </div>
      </div>
      <div class="w-row home-video-facebook">
        <div class="w-col w-col-8 full-width-column w-col-stack">
          <div class="w-row">
            <div class="home-video-container w-hidden-small w-hidden-tiny">
              <p>
              <img src="<?php bloginfo("template_directory"); ?>/images/home-play-bt.png">
                See what high schools across the country are saying about us:
              </p>
              <div class="w-embed w-video" style="padding-top: 56.20608899297424%;">
                <iframe class="embedly-embed" src="https://cdn.embedly.com/widgets/media.html?src=https%3A%2F%2Fwww.youtube.com%2Fembed%2FYOlSfZJ-A28%3Ffeature%3Doembed&amp;url=http%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DYOlSfZJ-A28&amp;image=http%3A%2F%2Fi1.ytimg.com%2Fvi%2FYOlSfZJ-A28%2Fhqdefault.jpg&amp;key=c4e54deccf4d4ec997a64902e9a30300&amp;type=text%2Fhtml&amp;schema=youtube"
                scrolling="no" frameborder="0" allowfullscreen=""></iframe>
              </div>
            </div>
          </div>
          <div class="w-row home-basketbal-section">
            <blockquote class="testimonial testimonial-home-basket">
              <p>“I would like to extend our gratitude to Community All-Stars.  It has been a successful venture and a great benefit. Top Quality!”
                <br><strong>- Harold Tanaka, Farrington High School</strong>
              </p>
            </blockquote>
          </div>
        </div>
        <div class="w-col w-col-4 facebook-home-column">
          <div class="facebook-home-container w-hidden-medium w-hidden-small w-hidden-tiny">
            <h4 class="facebook-title">The Latest:</h4>
            <div class="w-embed w-iframe">
              <iframe src="https://www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fpages%2FCommunity-All-Stars%2F151063808300640&amp;width=250&amp;height=440&amp;colorscheme=light&amp;show_faces=true&amp;header=true&amp;stream=true&amp;show_border=true&amp;appId=435779819838961" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:250px; height:440px; background-color: white;" allowtransparency="true"></iframe>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php get_template_part( 'video', 'modal' ); ?>
<?php get_template_part( 'footer', 'home' ); ?>