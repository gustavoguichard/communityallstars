jQuery(document).ready(function() {
  var $ = window.jQuery;
  $("a[rel*=leanModal]").leanModal().click(function(){
    return false;
  });
  $(".faq-list-item").click(function(event) {
    $(this).find('.faq-answer').slideToggle();
  });
  $slider = $('#playerSlider')
  if($slider.length > 0){
    $slider.cycle({ 
        fx:    'fade', 
        speed:  500,
        timeout: 6000
     });
  }
});