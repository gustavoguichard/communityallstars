<?php /** Template Name: About Page*/ get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
  <div class="hero-section">
    <div class="w-container hero hero-home">
      <img class="w-hidden-tiny football-player" src="<?php bloginfo("template_directory"); ?>/images/player.png" alt="Football Player" data-ix="player-appearing">
      <header class="w-row">
        <div class="w-col w-col-7">
          <h1 class="w-clearfix hero-call" data-ix="text-appearing">We’re all about <strong class="important">High School Sports.</strong></h1>
          <p class="hero-paragraph" data-ix="text-appearing-2">Designed exclusively for high school athletics, our services are free to high schools nationwide.</p>
        </div>
        <div class="w-col w-col-5"></div>
      </header>
    </div>
  </div>
  <div class="main-content section">
    <div class="w-container">
      <div class="w-row main-row">
        <div class="w-col w-col-4">
          <h3 class="about-heading">Welcome to the team.</h3>
          <p class="about-paragraph">Dedicated to community involvement and student success, we have created a highly effective, win-win system aimed to help alleviate the budget constraints taking place in high school athletics. With an accredited “A” rating by the Better Business
            Bureau and over 40 years of combined experience, we are the leader in scholastic athletic branding, marketing and funding. In a time when budgets are being cut left and right, our goal is to help sustain and promote high school athletics.
            We believe high school sports should remain free to students nationwide. Our services and everything we do is 100% free to high schools nationwide.</p>
        </div>
        <div class="w-col w-col-4">
          <h3 class="about-heading">What we do.</h3>
          <p class="about-paragraph">We market, brand and fund high school athletic teams and departments as if they were a professional franchise. With a full line of digital and print media and products, our service has become increasingly popular for high schools across the
            country. We are full service and take care of everything from start to finish so the schools can focus on what they do best. We are directly contracted with schools across the country and everything we do and create is free.</p>
        </div>
        <div class="w-col w-col-4">
          <h3 class="about-heading">Who we are.</h3>
          <p class="about-paragraph">Our team is made up of branding and marketing experts, professional designers and developers, seasoned sponsorship representatives and business development leaders. With a combined 40 + years of experience we have what it takes to accomplish
            what needs to be done.</p>
        </div>
      </div>
      <div class="w-row main-row">
        <div class="w-col w-col-5 w-col-medium-6 w-col-small-6">
          <p>See what high schools across the country are saying about us:</p>
          <div class="w-embed w-video" style="padding-top: 56.20608899297424%;">
            <iframe class="embedly-embed" src="https://cdn.embedly.com/widgets/media.html?src=https%3A%2F%2Fwww.youtube.com%2Fembed%2FYOlSfZJ-A28%3Ffeature%3Doembed&amp;url=http%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DYOlSfZJ-A28&amp;image=http%3A%2F%2Fi1.ytimg.com%2Fvi%2FYOlSfZJ-A28%2Fhqdefault.jpg&amp;key=c4e54deccf4d4ec997a64902e9a30300&amp;type=text%2Fhtml&amp;schema=youtube"
            scrolling="no" frameborder="0" allowfullscreen=""></iframe>
          </div>
        </div>
        <div class="w-col w-col-7 w-col-medium-6 w-col-small-6 w-clearfix">
          <div class="transparent-box">
            <a href="http://www.bbb.org/san-diego/business-reviews/fund-raising-counselors-and-orgs/community-all-stars-llc-in-san-diego-ca-171997569/" target="_blank"><img class="inline" src="<?php bloginfo("template_directory"); ?>/images/bbb.png"></a>
            <p class="inline">Proud to be accredited with an “A” rating by the Better Business Bureau.
            </p>
          </div>
          <img class="right from-game-time" src="<?php bloginfo("template_directory"); ?>/images/from-game-time.png" width="208">
        </div>
      </div>
    </div>
  </div>
<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>
