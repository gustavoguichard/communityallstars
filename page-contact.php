<?php /** Template Name: Contact Page*/ get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
  <div class="hero-section">
    <div class="w-container hero">
      <img class="w-hidden-tiny football-player" src="<?php bloginfo("template_directory"); ?>/images/player_contact.png" alt="Football Player" data-ix="player-appearing">
      <header class="w-row">
        <div class="w-col w-col-7">
          <h1 class="hero-call hero-inner-call" data-ix="text-appearing">Contact Us</h1>
          <p class="hero-paragraph hero-inner-paragraph" data-ix="text-appearing-2">We’re here to help.</p>
        </div>
        <div class="w-col w-col-5"></div>
      </header>
    </div>
  </div>
  <div class="main-content section">
    <div class="w-container">
      <div class="w-row main-row">
        <div class="w-col w-col-4 w-col-small-4 w-clearfix">
          <div class="block-column red-block full-height-column">
            <h2 class="block-title">Community All-Stars</h2><address><p class="block-address">710 13th St.<br>Suite 315<br>San Diego, Ca 92101</p></address>
            <a href="http://www.bbb.org/san-diego/business-reviews/fund-raising-counselors-and-orgs/community-all-stars-llc-in-san-diego-ca-171997569/" target="_blank"><img src="<?php bloginfo("template_directory"); ?>/images/bbb_small.png"></a>
          </div>
        </div>
        <div class="w-col w-col-4 w-col-small-4 w-clearfix block-column-wrapper">
          <div class="full-height-column block-column red-block">
            <p>Main Number
              <br><strong class="strong-number">866-558-1047</strong>
              <br>
              <br>Sponsor Line
              <br><strong class="strong-number">866-558-1042</strong>
            </p>
          </div>
        </div>
        <div class="w-col w-col-4 w-col-small-4 w-clearfix">
          <div class="block-column dark-block full-height-column">
            <div class="w-form">
              <?php the_content();?>
              <div class="w-form-done">
                <p>Thank you! Your submission has been received!</p>
              </div>
              <div class="w-form-fail">
                <p>Oops! Something went wrong while submitting the form :(</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="w-container main-row">
      <h3 class="red-block-title">Directory:</h3>
      <div class="block-column red-block">
        <div class="w-row">
          <div class="w-col w-col-4">
            <div class="directory-block">
              <h4 class="directory-title">Community Relations:</h4>
              <p><strong>Lauren Forbes</strong>
                <br>Administrative Director
                <br>PH: 866-558-1047
                <br>EXTENSION: 128 or Option 4
                <br>Lauren@communityallstars.com</p>
            </div>
          </div>
          <div class="w-col w-col-4">
            <div class="directory-block">
              <h4 class="directory-title">School Relations:</h4>
              <p><strong>Jeremiah Giddings</strong>
                <br>School Relations Director
                <br>PH: 866-558-1047
                <br>EXTENSION: 109 or Option 3
                <br>Jeremiah@communityallstars.com
              </p>
            </div>
          </div>
          <div class="w-col w-col-4">
            <div class="directory-block">
              <h4 class="directory-title">Design:</h4>
              <p><strong>Eric Holthaus</strong>
                <br>Design Director
                <br>PH: 866-558-1047
                <br>EXTENSION: 105 or Option 2
                <br>Ads@communityallstars.com
              </p>
            </div>
          </div>
        </div>
        <div class="w-row">
          <div class="w-col w-col-4">
            <div class="directory-block">
              <h4 class="directory-title">Management:</h4>
              <p><strong>Brett Benito</strong>
                <br>General Manager
                <br>PH: 866-558-1047
                <br>EXTENSION: 126
                <br>Brett@communityallstars.com</p>
            </div>
          </div>
          <div class="w-col w-col-4">
            <div class="directory-block">
              <h4 class="directory-title">Accounts:</h4>
              <p><strong>Angelica Cervantes</strong>
                <br>Account Manager
                <br>PH: 866-558-1047
                <br>EXTENSION: 127 or Option 6
                <br>Sponsorships@communityallstars.com
              </p>
            </div>
          </div>
          <div class="w-col w-col-4">
            <div class="directory-block">
              <h4 class="directory-title">Sponsorships:</h4>
              <p>Community All-Stars employs many sponsorship reps. To reach a specific sponsor rep, call 866-558-1042 and ask for him/her.</p>
            </div>
          </div>
        </div>
        <div class="w-row">
          <div class="w-col w-col-4">
            <div class="directory-block">
              <h4 class="directory-title">Executive:</h4>
              <p><strong>Michael Eisen</strong>
                <br>CEO / Founder
                <br>PH: 866-558-1047
                <br>EXTENSION: 131
                <br>Mike@communityallstars.com</p>
            </div>
          </div>
          <div class="w-col w-col-4">
            <div class="directory-block">
              <h4 class="directory-title">Employment:</h4>
              <p><strong>Brett Benito</strong>
                <br>General Manager
                <br>PH: 866-558-1047
                <br>EXTENSION: 126 or Option 3 Brett@communityallstars.com
              </p>
            </div>
          </div>
          <div class="w-col w-col-4">
            <div class="directory-block">
              <h4 class="directory-title">Partnerships and Media:</h4>
              <p><strong>Michael Eisen</strong>
                <br>CEO / Founder
                <br>PH: 866-558-1047
                <br>EXTENSION: 131
                <br>Mike@communityallstars.com</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>
