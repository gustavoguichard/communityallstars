<?php /** Template Name: Our Work Page*/ get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
  <div class="hero-section">
    <div class="w-container hero">
      <img class="w-hidden-tiny football-player" src="<?php bloginfo("template_directory"); ?>/images/player_contact.png" alt="Football Player" data-ix="player-appearing">
      <header class="w-row">
        <div class="w-col w-col-7">
          <h1 class="hero-call hero-inner-call" data-ix="text-appearing">Our Work</h1>
          <p class="hero-paragraph hero-inner-paragraph" data-ix="text-appearing-2">A full line of professionally designed and produced media designed to boost your image, promote your teams, and generate the funds needed to help run your department. Everything we do is custom tailored and free to your teams and school.</p>
        </div>
        <div class="w-col w-col-5"></div>
      </header>
    </div>
  </div>
  <div class="section work-section white-work-section">
    <div class="w-container">
      <div class="w-row">
        <div class="w-col w-col-4">
          <h2 class="red-title-h2">Print Collateral</h2>
          <p class="our-work-paragraph">Our full line of print collateral includes posters, magazines, programs, schedule cards, banners, apparel and ticket stubs.
            <br>
            <br>Everything is custom tailored to each individual school or team and features pictures of the actual student athletes.</p>
          <h2 class="red-title-h2">Digital Collateral</h2>
          <p class="our-work-paragraph">A comprehensive selection of digital design and development including, web, TV, fundraising software and digital promotions.</p>
        </div>
        <div class="w-col w-col-8">
          <img src="<?php bloginfo("template_directory"); ?>/images/our-work1.png">
        </div>
      </div>
    </div>
  </div>
  <div class="section work-section gray-work-section">
    <div class="w-container">
      <div class="w-row">
        <div class="w-col w-col-9 column-with-bottom-space">
          <h2>Athletic&nbsp;Posters.</h2>
          <p class="our-work-paragraph">Our athletic posters are among our most popular products and are a great way to brand your teams, promote your games and create keepsake products for the parents and students.. Posters feature pictures of your student athletes, schedules for
            your teams and your school colors and logos.</p>
        </div>
        <div class="w-col w-col-3"></div>
      </div>
    </div>
    <div class="w-container centered">
      <img src="<?php bloginfo("template_directory"); ?>/images/our-work2.png">
      <div class="small-explanation">Click boxes below to enlarge samples.</div>
      <a href="#full-image-1" rel="leanModal"><img class="portfolio-thumb" src="<?php bloginfo("template_directory"); ?>/images/posters/poster-1-thumb.jpg"></a>
      <a href="#full-image-2" rel="leanModal"><img class="portfolio-thumb" src="<?php bloginfo("template_directory"); ?>/images/posters/poster-2-thumb.jpg"></a>
      <a href="#full-image-3" rel="leanModal"><img class="portfolio-thumb" src="<?php bloginfo("template_directory"); ?>/images/posters/poster-3-thumb.jpg"></a>
      <a href="#full-image-4" rel="leanModal"><img class="portfolio-thumb" src="<?php bloginfo("template_directory"); ?>/images/posters/poster-4-thumb.jpg"></a>
      <a href="#full-image-5" rel="leanModal"><img class="portfolio-thumb" src="<?php bloginfo("template_directory"); ?>/images/posters/poster-5-thumb.jpg"></a>
      <a href="#full-image-6" rel="leanModal"><img class="portfolio-thumb" src="<?php bloginfo("template_directory"); ?>/images/posters/poster-6-thumb.jpg"></a>
      <a href="#full-image-7" rel="leanModal"><img class="portfolio-thumb" src="<?php bloginfo("template_directory"); ?>/images/posters/poster-7-thumb.jpg"></a>
      <a href="#full-image-8" rel="leanModal"><img class="portfolio-thumb" src="<?php bloginfo("template_directory"); ?>/images/posters/poster-8-thumb.jpg"></a>
    </div>
  </div>
  <div class="section work-section white-work-section">
    <div class="w-container">
      <div class="w-row">
        <div class="w-col w-col-8 column-with-bottom-space">
          <h2>Magazines and Programs.</h2>
          <p class="our-work-paragraph">A magazine style publication featuring school info and articles, team schedules, action pictures of the students, stats, rosters, letters from the staff, and athlete bio’s.</p>
        </div>
        <div class="w-col w-col-4"></div>
      </div>
      <div class="w-row">
        <div class="w-col w-col-9">
          <img src="<?php bloginfo("template_directory"); ?>/images/our-work-magazines.png">
        </div>
        <div class="w-col w-col-3 centered">
          <img class="interactive-guide-icon" src="<?php bloginfo("template_directory"); ?>/images/red-doc.png"><a class="block body-link spaced-link" href="#issuu" rel="leanModal">Click to flip through our<br>interactive program.</a><a class="button" href="#issuu" rel="leanModal">Interactive Guide</a>
        </div>
      </div>
    </div>
  </div>
  <div class="section work-section gray-work-section">
    <div class="w-container">
      <div class="w-row">
        <div class="w-col w-col-7 w-col-small-7 column-with-bottom-space">
          <h2>Schedule Cards.</h2>
          <p class="our-work-paragraph">A wallet sized, bi-fold schedule card displays the schedule of an individual team, pictures of the team and an exclusive sponsor ad.</p>
        </div>
        <div class="w-col w-col-1 w-col-small-1"></div>
        <div class="w-col w-col-4 w-col-small-4">
          <h2>Apparel.</h2>
          <p class="our-work-paragraph">Custom designed apparel featuring your school and team logos, as well as a sponsor on the back.</p>
        </div>
      </div>
      <div class="w-row">
        <div class="w-col w-col-8 w-col-small-8 column-with-bottom-space">
          <img src="<?php bloginfo("template_directory"); ?>/images/our-work3.png">
        </div>
        <div class="w-col w-col-4 w-col-small-4">
          <img src="<?php bloginfo("template_directory"); ?>/images/our-work-ts.png">
        </div>
      </div>
    </div>
  </div>
  <div class="section work-section white-work-section">
    <div class="w-container">
      <div class="w-row">
        <div class="w-col w-col-4 w-col-small-4 column-with-bottom-space">
          <h3>Web Design &amp; Development&nbsp;</h3>
          <p class="our-work-paragraph">Create a custom designed and developed website for your athletic team or departments.</p>
        </div>
        <div class="w-col w-col-4 w-col-small-4">
          <h3>CAS Tv</h3>
          <p class="our-work-paragraph">Professionally edited, high definition promotional videos for your teams. Great to share on social media to pump up the base.</p>
        </div>
        <div class="w-col w-col-4 w-col-small-4">
          <h3>Fundraising Software.</h3>
          <p class="our-work-paragraph">A streamlined, fully automated easy to use and effective fundraising platform designed exclusively for team sports.</p><a class="body-link" href="http://www.teambank.org" target="_blank">visit www.teambank.org</a>
        </div>
      </div>
      <div class="w-row">
        <div class="w-col w-col-4 w-col-small-4">
          <img class="w-hidden-tiny ow-media-image owmi-even" src="<?php bloginfo("template_directory"); ?>/images/our-work-media1.png">
        </div>
        <div class="w-col w-col-4 w-col-small-4 centered">
          <img class="w-hidden-tiny ow-media-image" src="<?php bloginfo("template_directory"); ?>/images/our-work-media2.png">
        </div>
        <div class="w-col w-col-4 w-col-small-4">
          <img class="ow-media-image owmi-even" src="<?php bloginfo("template_directory"); ?>/images/our-work-media3.png">
        </div>
      </div>
    </div>
  </div>
  <div class="section work-section gray-work-section">
    <div class="w-container">
      <div class="w-row">
        <div class="w-col w-col-5 column-with-bottom-space">
          <h2>Custom Production.</h2>
          <p class="our-work-paragraph">We offer a comprehensive collection of custom produced, designed and manufactured products. Please contact us for a full list.</p><a class="w-hidden-small w-hidden-tiny body-link" href="<?php echo get_permalink_by_name('contact');?>">Contact Us</a>
        </div>
        <div class="w-col w-col-7">
          <blockquote class="testimonial testimonial-red-color">
            <p>“I would recommend every high school in the country use Community All-Stars.</p>
            <div class="testimonial-author">-Kevin Biggs, Mission Viejo High School</div>
          </blockquote>
        </div>
      </div>
    </div>
  </div>
  <div class="red-gray-section">
    <div class="w-container">
      <div class="w-row">
        <div class="w-col w-col-6">
          <div class="get-started-section get-started-today">
            <h2>Get Started Today.</h2>
            <p class="our-work-paragraph">To sign your teams or school up give our school relations department a call at 866-558-1047 option 3. To speak with our design team use option 2. Everything we do is 100% free to high schools nationwide.</p><a class="body-link black-link block"
            href="<?php echo get_permalink_by_name('contact');?>">Contact Us</a>
            <a href="http://www.bbb.org/san-diego/business-reviews/fund-raising-counselors-and-orgs/community-all-stars-llc-in-san-diego-ca-171997569/" target="_blank"><img class="bbb-get-started" src="<?php bloginfo("template_directory"); ?>/images/proud-to-be.png"></a>
          </div>
        </div>
        <div class="w-col w-col-6">
          <div class="w-form w-hidden-small w-hidden-tiny sponsors-form-wrapper footer-sponsors-form white-sponsor-form-wrapper">
            <form class="sponsors-form" id="wf-form-search-form" name="wf-form-search-form" role="search" method="get" action="<?php echo home_url( '/' ); ?>">
              <h2 class="sponsors-form-title footer-sponsor-form-title white-sponsor-form">To become a sponsor... <strong class="footer-sponsor-title">Find a School:</strong></h2>
              <input class="w-input input input-sponsors" id="s" type="text" placeholder="School Name" name="s">
              <input class="w-button submit-search submit-sponsors" type="submit" value="Find">
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="issuu" class="leanModalDiv">
    <div data-configid="12627725/8549498" style="width: 650px; height: 437px;" class="issuuembed"></div>
    <script type="text/javascript" src="//e.issuu.com/embed.js" async="true"></script>
  </div>
  <div id="full-image-1" class="leanModalDiv posterModal"><img src="<?php bloginfo("template_directory"); ?>/images/posters/poster-1-full.jpg"></div>
  <div id="full-image-2" class="leanModalDiv posterModal"><img src="<?php bloginfo("template_directory"); ?>/images/posters/poster-2-full.jpg"></div>
  <div id="full-image-3" class="leanModalDiv posterModal"><img src="<?php bloginfo("template_directory"); ?>/images/posters/poster-3-full.jpg"></div>
  <div id="full-image-4" class="leanModalDiv posterModal"><img src="<?php bloginfo("template_directory"); ?>/images/posters/poster-4-full.jpg"></div>
  <div id="full-image-5" class="leanModalDiv posterModal"><img src="<?php bloginfo("template_directory"); ?>/images/posters/poster-5-full.jpg"></div>
  <div id="full-image-6" class="leanModalDiv posterModal"><img src="<?php bloginfo("template_directory"); ?>/images/posters/poster-6-full.jpg"></div>
  <div id="full-image-7" class="leanModalDiv posterModal"><img src="<?php bloginfo("template_directory"); ?>/images/posters/poster-7-full.jpg"></div>
  <div id="full-image-8" class="leanModalDiv posterModal"><img src="<?php bloginfo("template_directory"); ?>/images/posters/poster-8-full.jpg"></div>
<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>
