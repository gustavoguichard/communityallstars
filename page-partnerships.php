<?php /** Template Name: Partnerships Page*/ get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
  <div class="hero-section">
    <div class="w-container hero">
      <img class="w-hidden-tiny football-player" src="<?php bloginfo("template_directory"); ?>/images/player_contact.png" alt="Football Player" data-ix="player-appearing">
      <header class="w-row">
        <div class="w-col w-col-7">
          <h1 class="hero-call hero-inner-call" data-ix="text-appearing">Partnerships</h1>
          <p class="hero-paragraph hero-inner-paragraph" data-ix="text-appearing-2">Coming soon.</p>
        </div>
        <div class="w-col w-col-5"></div>
      </header>
    </div>
  </div>
  <div class="main-content section">
    <div class="w-container"></div>
  </div>
<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>
