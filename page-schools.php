<?php /** Template Name: Schools Page*/ get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
  <div class="hero-section">
    <div class="w-container hero hero-home">
      <img class="w-hidden-tiny football-player" src="<?php bloginfo("template_directory"); ?>/images/player.png" alt="Football Player" data-ix="player-appearing">
      <header class="w-row">
        <div class="w-col w-col-7">
          <h1 class="hero-call hero-call-schools" data-ix="text-appearing"><strong class="important schools-important">Brand &amp; Fund</strong><br>Your Teams Like the Pros.</h1>
          <p class="hero-paragraph" data-ix="text-appearing-2">Designed exclusively for high school athletics, our services are free to high schools nationwide.</p>
        </div>
        <div class="w-col w-col-5"></div>
      </header>
    </div>
  </div>
  <div class="main-content section">
    <div class="w-container">
      <div class="w-row">
        <div class="w-col w-col-7">
          <blockquote class="testimonial">
            <p>“Community All-Stars has been a great partner to us... They exemplify the quality and professionalism that we expect. They’ve come through every time. I would recommend every high school in the country use Community All-Stars.”</p>
            <div class="testimonial-author">-Kevin Biggs, Mission Viejo High School</div><a class="body-link testimonial-link" href="#video">Watch Video</a>
          </blockquote>
        </div>
        <div class="w-col w-col-5">
          <div class="transparent-box">
            <a href="http://www.bbb.org/san-diego/business-reviews/fund-raising-counselors-and-orgs/community-all-stars-llc-in-san-diego-ca-171997569/" target="_blank"><img src="<?php bloginfo("template_directory"); ?>/images/bbb_trans.png"></a>
            <p>Proud to be accredited with an “A” rating by the Better Business Bureau.
            </p>
          </div>
        </div>
      </div>
      <div class="soccer-girl-section red-section">
        <h2 class="soccer-girl-title">The leaders in scholastic athletic<br>branding, marketing and funding.</h2>
        <p class="soccer-girl-paragraph">Our services are completely free to high schools nationwide... With a combined 40 years of experience, we handle everything from start to finish.</p>
        <p class="soccer-girl-paragraph">A full line of professionally designed and produced media designed to boost your image, promote your teams, and generate the funds needed to help run your department.</p>
        <ul class="soccer-section-list soccer-list-schools">
          <li class="soccer-list-item soccer-girl-list-schools">3 rounds of products per year and 3 financial returns per year...</li>
          <li class="soccer-list-item soccer-girl-list-schools">Our best schools receive over $10,000 a year from us.</li>
          <li class="soccer-list-item soccer-girl-list-schools">Our services and products are free to high schools nationwide.</li>
          <li class="soccer-list-item soccer-girl-list-schools">We are accredited with an “A” rating by the Better Business Bureau.</li>
        </ul>
        <img class="w-hidden-small w-hidden-tiny soccer-girl" src="<?php bloginfo("template_directory"); ?>/images/soccer-player.png">
      </div>
    </div>
  </div>
  <div class="section main-content video-section-schools">
    <div class="w-container">
      <div class="w-row">
        <div class="w-col w-col-6">
          <div class="padded-left-div dark-padded-left">
            <h3 class="peoples-title">See what high schools across the country are saying about us:</h3>
          </div>
          <div class="padded-left-div red-div">
            <img class="w-hidden-tiny basketball-player" src="<?php bloginfo("template_directory"); ?>/images/basketball-player-small.png">
            <h4>Sign your teams up today to take your department to the next level.</h4>
            <p class="our-work-paragraph">Contact our school relations department at 866-558-1047 option 3 to sign up. 100% Free.</p>
          </div>
        </div>
        <div class="w-col w-col-6 video-column" id="video">
          <div class="w-embed w-video" style="padding-top: 56.20608899297424%;">
            <iframe class="embedly-embed" src="https://cdn.embedly.com/widgets/media.html?src=https%3A%2F%2Fwww.youtube.com%2Fembed%2FYOlSfZJ-A28%3Ffeature%3Doembed&amp;url=http%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DYOlSfZJ-A28&amp;image=http%3A%2F%2Fi1.ytimg.com%2Fvi%2FYOlSfZJ-A28%2Fhqdefault.jpg&amp;key=c4e54deccf4d4ec997a64902e9a30300&amp;type=text%2Fhtml&amp;schema=youtube"
            scrolling="no" frameborder="0" allowfullscreen=""></iframe>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="red-section">
    <div class="w-container">
      <h3 class="features-title">Features of our Service</h3>
      <div class="w-row">
        <div class="w-col w-col-4 full-width-column features-section">
          <img class="features-image" src="<?php bloginfo("template_directory"); ?>/images/features-1.jpg">
          <div class="features-content">
            <h3 class="features-content-title">Professional Design 
and Production</h3>
            <p class="features-paragraph">We employ a full team of professional designers, developers and production assistants to ensure we deliver an image and product your teams deserve and your competition will be scared of. Everything we do and create is custom tailored from
              the ground up to suit your teams, school and department.</p>
          </div>
        </div>
        <div class="w-col w-col-4 full-width-column features-section features-odd">
          <img class="features-image" src="<?php bloginfo("template_directory"); ?>/images/features2.jpg">
          <div class="features-content">
            <h3 class="features-content-title">Funding</h3>
            <p class="features-paragraph">One of the main takeaways of our service is the financial return you will receive for successful campaigns. Through our proven systems and production, we have created a mechanism able to generate and deliver the funds you need to help run
              your teams and department. Our best schools receive over $10,000 a year from us.</p>
          </div>
        </div>
        <div class="w-col w-col-4 full-width-column features-section">
          <img class="features-image" src="<?php bloginfo("template_directory"); ?>/images/features3.jpg">
          <div class="features-content">
            <h3 class="features-content-title">Non Intrusive Approach</h3>
            <p class="features-paragraph">Dedicated to integrity and ethics, we have assembles an experienced sponsorship department that is able to effectively acquire sponsorships without intruding in to your community. Our track record speaks for itself with over 35,000 happy and
              satisfied sponsors. We represent you with the utmost professional standards which designed to ensure complete school and sponsor satisfaction.</p>
          </div>
        </div>
      </div>
      <div class="w-row">
        <div class="w-col w-col-4 full-width-column features-section">
          <img class="features-image" src="<?php bloginfo("template_directory"); ?>/images/features4.jpg">
          <div class="features-content">
            <h3 class="features-content-title">Corporate Sponsorships</h3>
            <p class="features-paragraph">We work with corporate sponsors across the country to help enhance our school campaigns. We have access to corporate sponsors that would otherwise not have the ability to access your campaign. In many cases when schools sign with us, they
              are doing so with corporate sponsors already on board. This greatly helps us generate additional funds for your teams.</p>
          </div>
        </div>
        <div class="w-col w-col-4 full-width-column features-section features-odd">
          <img class="features-image" src="<?php bloginfo("template_directory"); ?>/images/features5.jpg">
          <div class="features-content">
            <h3 class="features-content-title">Additional Benefits</h3>
            <p class="features-paragraph">Our services comes with a ton of additional benefits. At the end of every campaign we will deliver to you a list of businesses willing to donate products and services to your school completely free of charge. We also deliver to you products
              the parents and students love which in return make you look good and provides them with keepsake products. Take your athletic department to the next level, heighten school spirit and increase attendance at games.</p>
          </div>
        </div>
        <!-- <div class="w-col w-col-4">
          <div class="faq-section faq-schools">
            <img src="<?php bloginfo("template_directory"); ?>/images/question_white.png">
            <h3 class="inline">A few common questions:</h3>
            <ul class="faq-list">
              <li class="faq-list-item">
                Why Community All-Stars?
                <div class="faq-answer">
                  <p>High school across the country directly contract us because they simply do not have the time and resources to do such a program. We do this in bulk for high schools all across the country which allows us to ensure a beneficial and efficient system for schools to take advantage of.</p>
                </div>
              </li>
              <li class="faq-list-item">
                How does it work?
                <div class="faq-answer">
                  <p>Becoming a sponsor of your local high school is an extremely easy and quick process.  Once signed up you will receive a sponsor packet in the mail and our designers will custom create your sponsorship advertisement for you free of charge.  They will proof the ad with you before production and you are free to use the ad they create in any other advertising campaigns you may do.</p>
                </div>
              </li>
              <li class="faq-list-item">
                Where will my sponsorship be seen?
                <div class="faq-answer">
                  <p>Your sponsorship ad will be seen by thousands of eyes... The products you are featured on are passed out to the students, parents, fans and faculty of the school.  They are hung throughout the school and in various community centers around the community.  They are also hung in the students houses as a way to keep track of the teams schedules.</p>
                </div>
              </li>
              <li class="faq-list-item">
                What are my options for my ad artwork?
                <div class="faq-answer">
                  <p>You can pretty much put whatever you would like on your advertisement. Our designers will custom create your ad for you if you would like and proof it with you before print. Free of charge...</p>
                </div>
              </li>
              <li class="faq-list-item">
                Why do we work a season in advance?
                <div class="faq-answer">
                  <p>We work a season in advance in order to assure all production is done before the season starts and in order to get the funds to the teams in a timely manner.</p>
                </div>
              </li>
              <li class="faq-list-item">
                What is the target market demographics?
                <div class="faq-answer">
                  <p>You will be directly showing your support for your local high school to the students. parents, fans and faculty of the school.</p>
                </div>
              </li>
            </ul>
          </div>
        </div> -->
      </div>
    </div>
  </div>
  <div class="section main-content video-section-schools">
    <div class="w-container black-text centered">
      <img src="<?php bloginfo("template_directory"); ?>/images/portfolio-icon.png" width="90">
      <h2>Some of our work:</h2>
      <p class="ow-paragraph-centered">A full suite of custom designed, professionally created, manufactured and produced print and digital collateral and media creates a unique brand for your team which in return helps generate the funding you need to run your teams and department.
        Everything we do and create is custom tailored and free to your teams.</p>
      <img src="<?php bloginfo("template_directory"); ?>/images/portfolio-image.jpg">
      <div class="portfolio-thumbs">
        <a href="#full-image-1" rel="leanModal"><img class="portfolio-thumb" src="<?php bloginfo("template_directory"); ?>/images/posters/poster-1-thumb.jpg"></a>
      <a href="#full-image-2" rel="leanModal"><img class="portfolio-thumb" src="<?php bloginfo("template_directory"); ?>/images/posters/poster-2-thumb.jpg"></a>
      <a href="#full-image-3" rel="leanModal"><img class="portfolio-thumb" src="<?php bloginfo("template_directory"); ?>/images/posters/poster-3-thumb.jpg"></a>
      <a href="#full-image-4" rel="leanModal"><img class="portfolio-thumb" src="<?php bloginfo("template_directory"); ?>/images/posters/poster-4-thumb.jpg"></a>
      <a href="#full-image-5" rel="leanModal"><img class="portfolio-thumb" src="<?php bloginfo("template_directory"); ?>/images/posters/poster-5-thumb.jpg"></a>
      <a href="#full-image-6" rel="leanModal"><img class="portfolio-thumb" src="<?php bloginfo("template_directory"); ?>/images/posters/poster-6-thumb.jpg"></a>
      <a href="#full-image-7" rel="leanModal"><img class="portfolio-thumb" src="<?php bloginfo("template_directory"); ?>/images/posters/poster-7-thumb.jpg"></a>
      <a href="#full-image-8" rel="leanModal"><img class="portfolio-thumb" src="<?php bloginfo("template_directory"); ?>/images/posters/poster-8-thumb.jpg"></a>
      </div>
      <h2>View our Portfolio:</h2>
      <p>To see a deeper collection of our work please view our portfolio.</p><a class="button" href="<?php echo get_permalink_by_name('our-work');?>">Portfolio</a>
    </div>
  </div>
  <div class="gray-section section">
    <div class="w-container">
      <div class="w-row">
        <div class="w-col w-col-6 column-with-bottom-space">
          <h5>School Pages:</h5>
          <p class="our-work-paragraph ow-schools-paragraph">Every school or team is outfitted with a “school page” on our website to increase transparency, ease of sponsorships and visibility. Sponsors can see last seasons products, pick and ad size and pay all in once central location. This helps increase
            our efficiency, results and security.</p>
        </div>
        <div class="w-col w-col-6">
          <h5>We handle everything so you can focus on whats important:</h5>
          <p class="our-work-paragraph ow-schools-paragraph">Our job is to make you look good, deliver products the parents and students love, and generate funds you need to run your department. We are full service and handle everything from start to finish so you can focus on what is most important.</p>
        </div>
      </div>
    </div>
  </div>
  <div class="testimonials-school-section">
    <div class="w-container">
      <h3>A FEW TESTIMONIALS:</h3>
      <div class="w-row testimonials-row">
        <div class="w-col w-col-4 w-col-stack">
          <div class="w-embed w-video w-hidden-medium w-hidden-small w-hidden-tiny" style="padding-top: 56.20608899297424%;">
            <iframe class="embedly-embed" src="https://cdn.embedly.com/widgets/media.html?src=https%3A%2F%2Fwww.youtube.com%2Fembed%2FYOlSfZJ-A28%3Ffeature%3Doembed&amp;url=http%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DYOlSfZJ-A28&amp;image=http%3A%2F%2Fi1.ytimg.com%2Fvi%2FYOlSfZJ-A28%2Fhqdefault.jpg&amp;key=c4e54deccf4d4ec997a64902e9a30300&amp;type=text%2Fhtml&amp;schema=youtube"
            scrolling="no" frameborder="0" allowfullscreen=""></iframe>
          </div>
        </div>
        <div class="w-col w-col-4 w-col-stack">
          <blockquote class="testimonial">
            <p>Community All-Stars does a fantastic job with our seasonal sports schedule posters for Ardrey Kell High School.&nbsp; The color and format are very sharp and displays pictures from our photographer onto the poster making it a wonderful vehicle
              for our AK student-athletes.&nbsp; The advertisers and community are very pleased with the format, and we receive a nice reimbursement from them after each season’s schedule posters have been delivered.&nbsp;</p>
            <div class="testimonial-author">-&nbsp;Cheryl Feeney, Ardrey Kell HS, Athletic Director</div>
          </blockquote>
        </div>
        <div class="w-col w-col-4 w-col-stack">
          <blockquote class="testimonial">
            <p>“I would recommend every high school in the country use Community All-Stars.</p>
            <div class="testimonial-author">- Kevin Biggs, Mission Viejo High School</div>
          </blockquote>
        </div>
      </div>
      <div class="w-row testimonials-row">
        <div class="w-col w-col-4">
          <blockquote class="testimonial">
            <p>The product of communicating our Fall Sports Events through a sports calendar is a great success. &nbsp;James Campbell High School looks forward in continuing our relationship with Community All-Stars! Incredible job guys!</p>
            <div class="testimonial-author">- Samuel Delos Reyes &nbsp;CAA
              <br>Athletic Administrator
              <br>James Campbell High School</div>
          </blockquote>
        </div>
        <div class="w-col w-col-4">
          <blockquote class="testimonial">
            <p>"Community All-Stars has made PV Driving School known in school systems that would otherwise not know us. What good is it to be great driving instructors if no one knows you? Due to their service, our business has doubled! And on top of that,
              we get to help out the kids!”</p>
            <div class="testimonial-author">- Edward H. Costello, PV Driving School, New Jersey</div>
          </blockquote>
        </div>
        <div class="w-col w-col-4">
          <blockquote class="testimonial">
            <p>“I would like to extend our gratitude to Community All-Stars. It has been a successful venture and a great benefit. Top Quality!”</p>
            <div class="testimonial-author">- Harold Tanaka, Farrington High School</div>
          </blockquote>
        </div>
      </div>
    </div>
  </div>
  <div class="red-section our-process-section">
    <div class="w-container">
      <h4 class="our-process-title">Our Process:</h4>
      <div class="w-row">
        <div class="w-col w-col-3 w-col-small-6">
          <div class="process-content">
            <div class="circle-title">
              <h4>Education</h4>
            </div>
            <p>Our staff is educated about your school/department and determines the best approach for a campaign.</p>
          </div>
        </div>
        <div class="w-col w-col-3 w-col-small-6">
          <div class="process-content">
            <div class="circle-title">
              <h4>Lead Development</h4>
            </div>
            <p>We target what we believe to be the best potential leads for advertisers/sponsors and start to roll reach out to our national corporate sponsors.</p>
          </div>
        </div>
        <div class="w-col w-col-3 w-col-small-6">
          <div class="process-content">
            <div class="circle-title">
              <h4>Campaign</h4>
            </div>
            <p>Our expert staff of 25+ sponsor reps starts to reach out and acquire local and corporate sponsors / advertisers.</p>
          </div>
        </div>
        <div class="w-col w-col-3 w-col-small-6">
          <div class="process-content">
            <div class="circle-title">
              <h4>Product Design &amp; Development</h4>
            </div>
            <p>Our designers and developers begin to produce the products and ad artwork.</p>
          </div>
        </div>
      </div>
      <div class="w-row">
        <div class="w-col w-col-3 w-col-small-6">
          <div class="process-content">
            <div class="circle-title">
              <h4>Proofing</h4>
            </div>
            <p>All products are sent to you prior to production to ensure accuracy and satisfaction.</p>
          </div>
        </div>
        <div class="w-col w-col-3 w-col-small-6">
          <div class="process-content">
            <div class="circle-title">
              <h4>Account Reconciliation</h4>
            </div>
            <p>Any sponsors/advertisers with balances are reconciled.</p>
          </div>
        </div>
        <div class="w-col w-col-3 w-col-small-6">
          <div class="process-content">
            <div class="circle-title">
              <h4>Shipping</h4>
            </div>
            <p>Products are shipped to the school and sponsors.</p>
          </div>
        </div>
        <div class="w-col w-col-3 w-col-small-6">
          <div class="process-content">
            <div class="circle-title">
              <h4>Funding</h4>
            </div>
            <p>A financial return check is sent in the mail to you to help fund your teams.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="working-with-us-section">
    <div class="w-container">
      <div class="w-row red-row">
        <div class="w-col w-col-8 full-width-column">
          <div class="wwu-red-section">
            <h2>Working with us is as easy as:</h2>
          </div>
          <div class="wwu-white-section">
            <h4 class="wwu-title"><strong class="wwu-number">1.</strong> Signing Up</h4>
            <p>Signing your school or team up is a quick and easy process. Simply give our school relations department a call at 866-558-1047 option 3. You can also connect with us via email at Brett@communityallstars.com.</p>
            <h4 class="wwu-title"><strong class="wwu-number">2.</strong> Submit Pics and Schedules</h4>
            <p>Send us your pictures and schedules. We can also arrange a photographer to come out and take high quality pictures for you.</p>
            <h4 class="wwu-title"><strong class="wwu-number">3.</strong> Receive Products and Funds</h4>
            <p>You will receive your products prior to your season start.</p>
          </div>
        </div>
        <div class="w-col w-col-4 full-width-column">
          <div class="wwu-dark">
            <h2>Get Started Today.</h2>
            <p>Give our school relations department a call at
              <br>866-558-1047 option 3.</p><a class="body-link" href="<?php echo get_permalink_by_name('contact');?>">Contact Us</a>
          </div>
          <div class="w-clearfix wwu-cas">
            <img class="copyright-child wwu-logo" src="<?php bloginfo("template_directory"); ?>/images/cas_logo.png" width="42">
            <h1 class="logo-header wwu-logo-header">Community All-Stars</h1>
            <p>The nations leader in scholastic athletic branding and funding.</p>
            <a href="http://www.bbb.org/san-diego/business-reviews/fund-raising-counselors-and-orgs/community-all-stars-llc-in-san-diego-ca-171997569/" target="_blank"><img class="left" src="<?php bloginfo("template_directory"); ?>/images/bbb.png"></a>
            <p>Proud to be accredited with an “A”&nbsp;rating.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="full-image-1" class="leanModalDiv posterModal"><img src="<?php bloginfo("template_directory"); ?>/images/posters/poster-1-full.jpg"></div>
  <div id="full-image-2" class="leanModalDiv posterModal"><img src="<?php bloginfo("template_directory"); ?>/images/posters/poster-2-full.jpg"></div>
  <div id="full-image-3" class="leanModalDiv posterModal"><img src="<?php bloginfo("template_directory"); ?>/images/posters/poster-3-full.jpg"></div>
  <div id="full-image-4" class="leanModalDiv posterModal"><img src="<?php bloginfo("template_directory"); ?>/images/posters/poster-4-full.jpg"></div>
  <div id="full-image-5" class="leanModalDiv posterModal"><img src="<?php bloginfo("template_directory"); ?>/images/posters/poster-5-full.jpg"></div>
  <div id="full-image-6" class="leanModalDiv posterModal"><img src="<?php bloginfo("template_directory"); ?>/images/posters/poster-6-full.jpg"></div>
  <div id="full-image-7" class="leanModalDiv posterModal"><img src="<?php bloginfo("template_directory"); ?>/images/posters/poster-7-full.jpg"></div>
  <div id="full-image-8" class="leanModalDiv posterModal"><img src="<?php bloginfo("template_directory"); ?>/images/posters/poster-8-full.jpg"></div>
<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>
