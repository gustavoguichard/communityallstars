<?php /** Template Name: Sponsors Page*/ get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
  <div class="hero-section">
    <div class="w-container hero hero-home hero-sponsors">
      <img class="w-hidden-tiny football-player" src="<?php bloginfo("template_directory"); ?>/images/player_sponsors.png" alt="Football Player" data-ix="player-appearing">
      <header class="w-row">
        <div class="w-col w-col-6">
          <h2 data-ix="text-appearing">A Community Based Approach to Sustaining High School Athletics and Promoting Local Business.</h2>
          <p class="hero-paragraph" data-ix="text-appearing-2">Designed exclusively for high school athletics, our services are free to high schools nationwide.</p>
          <div class="w-form w-hidden-small w-hidden-tiny sponsors-form-wrapper">
            <form class="sponsors-form" name="wf-form-search-form" role="search" method="get" action="<?php echo home_url( '/' ); ?>">
              <h2 class="sponsors-form-title">Find a School:</h2>
              <input class="w-input input input-sponsors" id="s" type="text" placeholder="School Name" name="s">
              <input class="w-button submit-search submit-sponsors" type="submit" value="Find">
            </form>
          </div>
        </div>
        <div class="w-col w-col-6"></div>
      </header>
    </div>
  </div>
  <div class="main-content section">
    <div class="w-container">
      <div class="w-row">
        <div class="w-col w-col-7">
          <blockquote class="testimonial">
            <p>“Community All-Stars has been a great partner to us... They exemplify the quality and professionalism that we expect. They’ve come through every time. I would recommend every high school in the country use Community All-Stars.”</p>
            <div class="testimonial-author">-Kevin Biggs, Mission Viejo High School</div><a class="body-link testimonial-link" href="#video" rel="leanModal">Watch Video</a>
          </blockquote>
        </div>
        <div class="w-col w-col-5">
          <div class="transparent-box">
            <a href="http://www.bbb.org/san-diego/business-reviews/fund-raising-counselors-and-orgs/community-all-stars-llc-in-san-diego-ca-171997569/" target="_blank"><img src="<?php bloginfo("template_directory"); ?>/images/bbb_trans.png"></a>
            <p>Proud to be accredited with an “A” rating by the Better Business Bureau.
            </p>
          </div>
        </div>
      </div>
      <div class="soccer-girl-section gray-section-soccer">
        <p class="soccer-gray-paragraph">In a time when high school athletic budgets are cut left and right, we have created a win-win community based system aimed to help alleviate the budget constraints and promote local business.</p>
      </div>
      <div class="soccer-girl-section red-section">
        <ul class="soccer-section-list">
          <li class="soccer-list-item">We are directly contracted by high schools and our services are free to high schools nationwide.</li>
          <li class="soccer-list-item">We brand and fund high school teams as if they were a professional franchise... Our sponsors are our team players.</li>
          <li class="soccer-list-item">Your sponsorship will help pay for necessities such as travel, uniform, equipment and player participation expenses.</li>
          <li class="soccer-list-item">Put your business inside the high school and throughout the community....and in the hands of the parent, students, faculty and fans of the high school.</li>
          <li class="soccer-list-item">- Give back directly to your local community.
            <br>- Gain access to a highly desired target audience.
            <br>- Put your business name inside the high school.
            <br>- Free custom ad design for all sponsors.
            <br>- Associate your business with a great cause.</li>
        </ul>
        <img class="w-hidden-small w-hidden-tiny soccer-girl" src="<?php bloginfo("template_directory"); ?>/images/soccer-player.png">
      </div>
    </div>
  </div>
  <div class="main-content section">
    <div class="w-container">
      <blockquote class="testimonial">
        <p>"Community All-Stars has made PV Driving School known in school systems that would otherwise not know us. What good is it to be great driving instructors if no one knows you? Due to their service, our business has almost doubled...and on top of
          that, we get to help out the kids!”</p>
        <div class="testimonial-author">- Edward H. Costello
          <br>PV Driving School, New Jersey</div>
      </blockquote>
    </div>
  </div>
  <div>
    <div class="w-container">
      <div class="w-row">
        <div class="w-col w-col-4">
          <div class="three-column-red">
            <div class="three-column-percent">94%</div>
            <p>Of high school sports fans feel it is important to be aware of which companies sponsor their teams.</p>
          </div>
        </div>
        <div class="w-col w-col-4">
          <div class="three-column-red">
            <div class="three-column-percent">72%</div>
            <p>Of those surveyed said they would rather see a company spend their sponsorship budget on high schools than professional sports teams.</p>
            <div class="source">Source: Turnkey Sports &amp; Entertainment 2010 HTM Survey (A18-60)</div>
          </div>
        </div>
        <div class="w-col w-col-4">
          <div class="three-column-red">
            <div class="three-column-percent">78%</div>
            <p>Of those surveyed feel that local high school sports have a positive influence on their community.</p>
          </div>
        </div>
      </div>
    </div>
    <div class="main-content section">
      <div class="w-container">
        <h3 class="red-block-title centered">Become a sponsor of your local high school today and gain valuable brand recognition while supporting the kids.</h3>
        <div class="sponsorship-adds">
          <div class="w-row gray-row">
            <div class="w-col w-col-4 green-gray-section">
              <div class="green-gray-content">
                <img src="<?php bloginfo("template_directory"); ?>/images/green_doc.png">
                <div class="green-gray-title">Sample Sponsorship Ads:</div>
                <div>We offer free professional design for all sponsorship ads.</div>
                <div class="adds-images">
                  <img class="add-image" src="<?php bloginfo("template_directory"); ?>/images/sample-sponsors/sample-add-07.png" alt="Sample Add" />
                  <img class="add-image" src="<?php bloginfo("template_directory"); ?>/images/sample-sponsors/sample-add-11.png" alt="Sample Add" />
                  <img class="add-image" src="<?php bloginfo("template_directory"); ?>/images/sample-sponsors/sample-add-05.png" alt="Sample Add" />
                  <img class="add-image" src="<?php bloginfo("template_directory"); ?>/images/sample-sponsors/sample-add-02.png" alt="Sample Add" />
                  <img class="add-image" src="<?php bloginfo("template_directory"); ?>/images/sample-sponsors/sample-add-03.png" alt="Sample Add" />
                  <img class="add-image" src="<?php bloginfo("template_directory"); ?>/images/sample-sponsors/sample-add-01.png" alt="Sample Add" />
                </div>
              </div>
            </div>
            <div class="w-col w-col-8 red-column">
              <div class="adds-content-section">
                <div class="w-row adds-red-row">
                  <div class="w-col w-col-6">
                    <div class="adds-red-content">
                      <img src="<?php bloginfo("template_directory"); ?>/images/adds-icon-1.png">
                      <h4 class="adds-red-title-h4">Giving Back:</h4>
                      <p class="adds-content-paragraph">In a time when high school athletic budgets are being slashed left and right, your sponsorship is truly needed more now than ever. Get brand recognition while being an active supporter of your local community and school system. High
                        school athletics have always been a main tool in promoting a healthy and positive direction for the youth of our country.</p>
                    </div>
                  </div>
                  <div class="w-col w-col-6">
                    <div class="adds-red-content">
                      <img src="<?php bloginfo("template_directory"); ?>/images/adds-icon-2.png">
                      <h4 class="adds-red-title-h4">Your Sponsorship:</h4>
                      <p class="adds-content-paragraph">Becoming a sponsor of your school is a very easy process. We have a variety of sponsor ad sizes to choose from and we offer free professional design services for all sponsors if they choose. By becoming a sponsors you are not only
                        supporting the kids with necessities such as travel, equipment, uniform and participation expenses but you are also putting your business inside the school and in the hands of the parents, students, faculty, fans and community
                        of the school.</p>
                    </div>
                  </div>
                </div>
                <div class="w-row adds-red-row">
                  <div class="w-col w-col-6">
                    <div class="adds-red-content">
                      <img src="<?php bloginfo("template_directory"); ?>/images/adds-icon-3.png">
                      <h4 class="adds-red-title-h4">Target Audience:</h4>
                      <p class="adds-content-paragraph">Your sponsorship ad will be seen by thousands of eyes within the school community. The parents, students, fans, faculty and community of the school. We offer a highly desired target audience with buying power.</p>
                    </div>
                  </div>
                  <div class="w-col w-col-6">
                    <div class="adds-red-content">
                      <img src="<?php bloginfo("template_directory"); ?>/images/adds-icon-4.png">
                      <h4 class="adds-red-title-h4">Distribution:</h4>
                      <p class="adds-content-paragraph">Your sponsorship ad will be distributed within the school system and community. They will be displayed a various community enterprises as well as in local businesses. They will be placed in classrooms and distributed to the parents
                        and students to hang at their house.</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="w-row dark-gray-row">
                <div class="w-col w-col-7 white-row">
                  <div class="add-products-section">
                    <h4>Product:</h4>
                    <p class="add-products-paragraph">Please view our portfolio to see a collection of work and products.</p>
                    <img class="add-products-image" src="<?php bloginfo("template_directory"); ?>/images/adds_media.png">
                  </div>
                </div>
                <div class="w-col w-col-5 add-portfolio-column">
                  <img src="<?php bloginfo("template_directory"); ?>/images/portfolio-icon.png">
                  <h2 class="add-portfolio-title">View our Portfolio:</h2>
                  <div>To see a deeper collection of our
                    <br>work please view our portfolio.</div>
                    <a class="button portfolio-button" href="<?php echo get_permalink_by_name('our-work');?>">Portfolio</a>
                </div>
              </div>
              <div class="add-sizes-section">
                <h4>Available Add Sizes:</h4>
                <p>Below are the available sponsorship ad sizes that we offer. We can do custom sizes upon request. All ads are on a first come first serve bases. Ads below are not true to size and show only proportions. All ad sizes are in inches.</p>
                <img src="<?php bloginfo("template_directory"); ?>/images/add_sizes.png" alt="Add Sizes">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="black-section black-section-player">
    <div class="w-container">
      <div class="w-row">
        <div class="w-col w-col-6">
          <div class="faq-section">
            <img src="<?php bloginfo("template_directory"); ?>/images/question_white.png">
            <h4 class="inline">A few common questions:</h4>
            <ul class="faq-list">
              <li class="faq-list-item">
                Why Community All-Stars?
                <div class="faq-answer">
                  <p>High school across the country directly contract us because they simply do not have the time and resources to do such a program. We do this in bulk for high schools all across the country which allows us to ensure a beneficial and efficient system for schools to take advantage of.</p>
                </div>
              </li>
              <li class="faq-list-item">
                How does it work?
                <div class="faq-answer">
                  <p>Becoming a sponsor of your local high school is an extremely easy and quick process.  Once signed up you will receive a sponsor packet in the mail and our designers will custom create your sponsorship advertisement for you free of charge.  They will proof the ad with you before production and you are free to use the ad they create in any other advertising campaigns you may do.</p>
                </div>
              </li>
              <li class="faq-list-item">
                Where will my sponsorship be seen?
                <div class="faq-answer">
                  <p>Your sponsorship ad will be seen by thousands of eyes... The products you are featured on are passed out to the students, parents, fans and faculty of the school.  They are hung throughout the school and in various community centers around the community.  They are also hung in the students houses as a way to keep track of the teams schedules.</p>
                </div>
              </li>
              <li class="faq-list-item">
                What are my options for my ad artwork?
                <div class="faq-answer">
                  <p>You can pretty much put whatever you would like on your advertisement. Our designers will custom create your ad for you if you would like and proof it with you before print. Free of charge...</p>
                </div>
              </li>
              <li class="faq-list-item">
                Why do we work a season in advance?
                <div class="faq-answer">
                  <p>We work a season in advance in order to assure all production is done before the season starts and in order to get the funds to the teams in a timely manner.</p>
                </div>
              </li>
              <li class="faq-list-item">
                What is the target market demographics?
                <div class="faq-answer">
                  <p>You will be directly showing your support for your local high school to the students. parents, fans and faculty of the school.</p>
                </div>
              </li>
            </ul>
          </div>
        </div>
        <div class="w-col w-col-6"></div>
      </div>
    </div>
  </div>
  <div>
    <div class="w-container">
      <div class="testimonials-section">
        <div class="w-row">
          <div class="w-col w-col-4">
            <blockquote class="red-testimonial">
              <p>“Our commitment in supporting our children should be a top priority . Thank you for your help in furthering this positive effort!”
                <br><strong>Jane Davilla, AJLC Preschool and Extended Daycare</strong>
              </p>
            </blockquote>
          </div>
          <div class="w-col w-col-4">
            <blockquote class="red-testimonial">
              <p>"Community All-Stars has made PV Driving School known in school systems that would otherwise not know us. What good is it to be great driving instructors if no one knows you? Due to their service, our business has doubled! And on top of
                that, we get to help out the kids!”
                <br><strong>- Edward H. Costello, PV Driving School, New Jersey</strong>
              </p>
            </blockquote>
          </div>
          <div class="w-col w-col-4">
            <blockquote class="red-testimonial">
              <p>“The poster looks amazing and we couldn't be happier with it... Thank you so much!”
                <br><strong>Liz Karam, NYSC</strong>
              </p>
            </blockquote>
          </div>
        </div>
        <div class="w-row">
          <div class="w-col w-col-4">
            <blockquote class="red-testimonial">
              <p>“The programs turned out much nicer than expected!”
                <br><strong>Grace Chen Ellis, Mandarin Coaching</strong>
              </p>
            </blockquote>
          </div>
          <div class="w-col w-col-4">
            <blockquote class="red-testimonial">
              <p>“We have placed ads on posters before but these are far better than anything from the past!”
                <br><strong>Wanda Siegmund, Rhythms Powder coating</strong>
              </p>
            </blockquote>
          </div>
          <div class="w-col w-col-4"></div>
        </div>
        <div class="w-row">
          <div class="w-col w-col-1"></div>
          <div class="w-col w-col-5">
            <h2>Who knows?<br>Maybe you are sponsoring the 
next superstar.</h2>
          </div>
          <div class="w-col w-col-6"></div>
        </div>
      </div>
    </div>
  </div>
  <div class="gray-white-section">
    <div class="w-container">
      <div class="w-row">
        <div class="w-col w-col-6">
          <div class="get-started-section">
            <h4>Get Started:</h4>
            <p>Becoming a sponsor for your local school is an extremely easy process that only takes a few minutes.</p>
            <h4>Give our sponsor line a call at<br><strong class="phone-strong">866-558-1042</strong></h4>
          </div>
        </div>
        <div class="w-col w-col-6">
          <div class="w-form w-hidden-small w-hidden-tiny sponsors-form-wrapper footer-sponsors-form">
            <form class="sponsors-form" name="wf-form-search-form" role="search" method="get" action="<?php echo home_url( '/' ); ?>">
              <h2 class="sponsors-form-title footer-sponsor-form-title">To become a sponsor... <strong class="footer-sponsor-title">Find a School:</strong></h2>
              <input class="w-input input input-sponsors" id="s" type="text" placeholder="School Name" name="s">
              <input class="w-button submit-search submit-sponsors" type="submit" value="Find">
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php get_template_part( 'video', 'modal' ); ?>
<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>
