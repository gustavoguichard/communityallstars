
<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 * * @package WordPress
 * @subpackage community-all-stars
 * @since community-all-stars 1.0
 */

get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<!-- Breadcump Start -->
<div style=" clear:both;"></div>
<!-- Breadcump End -->

<!-- Page Top Navi Start -->
<div class="container_16">
  <div class="grid_16" id="tabnavi-back-two">
    <div class="grid_11">
      <div class="pagenavi-title"><span class="pagenavi-h1"><?php the_title(); ?></span>
      <span class="pagenavi-h2">
      <?php if ( get_post_meta(get_the_ID(), 'title', true) ) { ?>
             <?php echo get_post_meta(get_the_ID(), "title", $single = true); ?>
             <?php } ?>
             <?php 
             if(is_page("school-pages")) {
             	echo "To find your school enter the name in the search box";
             } 
             ?>
      </span>
      </div>
    </div>
  </div>
</div>
<div style="clear:both;"></div>
<?php if(!is_page("school-pages")) { ?>
<div style="height:166px;width:100%;margin:0;padding:0;border:0;"></div>
<?php } 
	  else { ?>
<div style="height:50px;width:100%;margin:0;padding:0;border:0;"></div>
<?php } ?>
<!-- Page Top Navi End -->

<?php 
	if(is_page("about")) { $custom_bg = "custom_bg"; }
	elseif(is_page("schools")) { $custom_bg = "custom_bg"; }
	elseif(is_page("contact")) { $custom_bg = "custom_bg"; }
	elseif(is_page("sponsors")) { $custom_bg = "custom_bg"; }
	else { $custom_bg = false; }
?>

<!-- Blog and Siderbar Start -->
<div class="container_16">
	
	<?php if(is_page("about")) { ?>
	<div style="overflow:hidden;margin:260px auto 20px;width:920px;padding:10px;background: url('<?php bloginfo("template_directory"); ?>/images/bg-white.png') repeat top center;">
		<div style="width:300px;display:inline-block;margin:0 10px 0 0;padding:0;float:left;">
			<h3 style="margin:0 0 5px;padding:10px;display:block;background: url('<?php bloginfo("template_directory"); ?>/images/bg-h3.png') repeat top center;">Our Philosophy</h3>
			<div style="background:#1a1b20;padding:10px;height:270px;font-weight:normal;color:#fff;">Dedicated to community involvement and student success, CAS has created a win-win situation for everyone involved. We effectively and professionally market and brand high school athletic departments and help them generate the funds they need to stay afloat. We get the community involved and create highly effective advertising for the local businesses around the schools. We are directly contracted by each and every school we work with!</div>
		</div>
		<div style="width:300px;display:inline-block;margin:0 10px 0 0;padding:0;float:left;">
			<h3 style="margin:0 0 5px;padding:10px;display:block;background: url('<?php bloginfo("template_directory"); ?>/images/bg-h3.png') repeat top center;">Our Passion</h3>
			<div style="background:#1a1b20;padding:10px;height:270px;font-weight:normal;color:#fff;">Here at CAS, we are marketing and funding professionals who excel in building your fan base. Whether you are an athletic department looking to develop a recognizable brand or a local business searching for grassroots channels to make an imprint, CAS is the most effective creative marketing solution. By providing top quality products, customer service and design, we ensure that your campaign will get results. Join the team, build your brand and strengthen your ties to the local community.</div>
		</div>
		<div style="width:300px;display:inline-block;margin:0;padding:0;float:left;">
			<h3 style="margin:0 0 5px;padding:10px;display:block;background: url('<?php bloginfo("template_directory"); ?>/images/bg-h3.png') repeat top center;">Our Vision</h3>
			<div style="background:#1a1b20;padding:10px;height:270px;font-weight:normal;color:#fff;">With the help of the local communities, we believe we can sustain and build high school athletics to help ensure student success beyond the playing field.
			<br><br>
			<iframe width="280" height="170" src="https://www.youtube.com/embed/YOlSfZJ-A28" frameborder="0" allowfullscreen></iframe></div>
		</div>
  	</div>
  	
  	<div class="grid_16">
    	<div class="bloglist-panel">
      		<div class="blog-post">
       		 <p style="margin-top:-14px;"><?php the_content(); ?></p>
      		</div>
    	</div>    
  	</div>
  	
  	<?php } 
  	
  	elseif(is_page("school-pages")) { ?>
  	
		<!-- Start Blog List -->
	  <div class="grid_11" style="width:960px;">
	  
		<div id="home_search" style="margin:0 30px 0 0px;float:left;inline-block;height:170px;">
			<img src="<?php bloginfo("template_directory"); ?>/images/home/home_search_header.png">
			<form role="search" method="get" action="/">
				<input type="text" id="home_search_input" name="s" />
				<input type="submit" id="home_search_submit" />
			</form>
		</div>

	
		<div style="clear:both;"></div>
		
		<!-- Page Navi -->
		<div class="page-navi">
		  <?php posts_nav_link( ' ', '<img src="' . get_bloginfo('stylesheet_directory') . '/images/b2.png" />', '<img src="' . get_bloginfo('stylesheet_directory') . '/images/b1.png" />' ); ?>
		</div>    
		
	  </div>
	  
  	
  	<?php }

  	elseif(is_page("contact")) { ?>
  	
  	<div class="shadow" style="margin-top:100px;">
	  	<div style="width:960px;margin:0 auto;padding:0;background:#000;overflow:hidden;">
			<div style="overflow:hidden;height:360px;margin:0; float:left;width:550px;padding:10px 10px 10px 25px;background: #000 url('<?php bloginfo("template_directory"); ?>/images/contact-softball.png') no-repeat center right;">
				<h2 class="futurebold" style="font-size:36px;line-height:110%;color:#fff;margin-top:25px;">710 13th ST. Suite 315<br>San Diego, CA 92101</h2>
				<h2 class="futurebold" style="font-size:36px;line-height:110%;color:#fff;margin-top:30px;">Main Number:<Br>(866) 558-1047</h2>
				<h2 class="futurebold" style="font-size:36px;line-height:110%;color:#fff;margin-top:30px;">Sponsors Line:<Br>(866) 558-1042</h2>
			</div>
			<div style="width:345px;padding:15px;float:right;display:inline-block;">
				<iframe width="345" height="150" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=710+13th+ST.+Suite+315+San+Diego,+CA+92101&amp;aq=&amp;sll=37.0625,-95.677068&amp;sspn=56.243791,98.085938&amp;vpsrc=0&amp;ie=UTF8&amp;hq=&amp;hnear=710+13th+St,+San+Diego,+California+92101&amp;t=m&amp;ll=32.712922,-117.152967&amp;spn=0.012638,0.029526&amp;z=12&amp;iwloc=near&amp;output=embed"></iframe>
				<?php echo do_shortcode('[contact-form 1 "Contact form 1"]'); ?>
			</div>
		</div>
	</div>
	
	<div class="shadow">	
		<div style="clear:both;overflow:hidden;margin:0 auto;width:940px;padding:10px;background: url('<?php bloginfo("template_directory"); ?>/images/bg-white.png') repeat top center;">
			<div style="width:306px;display:inline-block;margin:0 10px 0 0;padding:0;float:left;">
				<h3 style="margin:0 0 5px;padding:10px;display:block;background: url('<?php bloginfo("template_directory"); ?>/images/bg-h3.png') repeat top center;">
					Community Relations
				</h3>
				<div style="background:#1a1b20;padding:10px;height:76px;font-weight:normal;color:#fff;">
					Lauren Forbes<Br>Administrative Director<br>EXTENSION: 128 or Option 4<br><a href="mailto:Lauren@communityallstars.com">Lauren@communityallstars.com</a>
				</div>
			</div>
			<div style="width:306px;display:inline-block;margin:0 10px 0 0;padding:0;float:left;">
				<h3 style="margin:0 0 5px;padding:10px;display:block;background: url('<?php bloginfo("template_directory"); ?>/images/bg-h3.png') repeat top center;">
					Accounts
				</h3>
				<div style="background:#1a1b20;padding:10px;height:76px;font-weight:normal;color:#fff;">
					Cynthia Montoya<br>Account Manager<br>EXTENSION: 127 or Option 6<br><a href="mailto:Sponsorships@communityallstars.com">Sponsorships@communityallstars.com</a>
				</div>
			</div>
			<div style="width:306px;display:inline-block;margin:0;padding:0;float:left;">
				<h3 style="margin:0 0 5px;padding:10px;display:block;background: url('<?php bloginfo("template_directory"); ?>/images/bg-h3.png') repeat top center;">
					School Relations
				</h3>
				<div style="background:#1a1b20;padding:10px;height:76px;font-weight:normal;color:#fff;">
					Brett Benito<br>School Relations Director<br>EXTENSION: 126 or Option 3<br><a href="mailto:Brett@communityallstars.com">Brett@communityallstars.com</a>
				</div>
			</div>
			<!-- -->
			<div style="width:306px;display:inline-block;margin:10px 10px 0 0;padding:0;float:left;">
				<h3 style="margin:0 0 5px;padding:10px;display:block;background: url('<?php bloginfo("template_directory"); ?>/images/bg-h3.png') repeat top center;">
					Graphic Design
				</h3>
				<div style="background:#1a1b20;padding:10px;height:76px;font-weight:normal;color:#fff;">
					Eric Holthaus<Br>Graphic Design Director<Br>EXTENSION: 105 or Option 2<br><a href="mailto:Ads@communityallstars.com">Ads@communityallstars.com</a>

				</div>
			</div>
			<div style="width:306px;display:inline-block;margin:10px 10px 0 0;padding:0;float:left;">
				<h3 style="margin:0 0 5px;padding:10px;display:block;background: url('<?php bloginfo("template_directory"); ?>/images/bg-h3.png') repeat top center;">
					Executive
				</h3>
				<div style="background:#1a1b20;padding:10px;height:76px;font-weight:normal;color:#fff;">
						Michael Eisen<Br>CEO / Founder<br>EXTENSION: 131<br><a href="mailto:Mike@communityallstars.com">Mike@communityallstars.com</a>
				</div>
			</div>
			<div style="width:306px;display:inline-block;margin:10px 0 0 0;padding:0;float:right;">
				<h3 style="margin:0 0 5px;padding:10px;display:block;background: url('<?php bloginfo("template_directory"); ?>/images/bg-h3.png') repeat top center;">
					Sponsorships
				</h3>
				<div style="background:#1a1b20;padding:10px;overflow:hidden;font-weight:normal;color:#fff;">
						Larry Reed<br>Project Coordinator<br>EXTENSION: 117 or Option 1<br><a href="mailto:Larry@communityallstars.com">Larry@communityallstars.com</a><br><Br>
						Frank Gutierrez<br>Project Coordinator<br>EXTENSION: 111 or Option 1<br><a href="mailto:Frank@communityallstars.com">Frank@communityallstars.com</a><br><Br>
						Community All-Stars employs many sponsorship reps. To reach a specific sponsor rep, call 866-558-1042 and ask for him/her.
				</div>
			</div>
			<!-- -->
			<div style="width:306px;display:inline-block;margin:10px 10px 0 0;padding:0;float:left;">
				<h3 style="margin:0 0 5px;padding:10px;display:block;background: url('<?php bloginfo("template_directory"); ?>/images/bg-h3.png') repeat top center;">
					Partnerships
				</h3>
				<div style="background:#1a1b20;padding:10px;height:76px;font-weight:normal;color:#fff;">
					Michael Eisen<Br>CEO / Founder<br>EXTENSION: 131<br><a href="mailto:Mike@communityallstars.com">Mike@communityallstars.com</a>
				</div>
			</div>
			<div style="width:306px;display:inline-block;margin:10px 10px 0 0;padding:0;float:left;">
				<h3 style="margin:0 0 5px;padding:10px;display:block;background: url('<?php bloginfo("template_directory"); ?>/images/bg-h3.png') repeat top center;">
					Employment
				</h3>
				<div style="background:#1a1b20;padding:10px;height:76px;font-weight:normal;color:#fff;">
						Brett Benito<Br>General Manager<br>EXTENSION: 126<Br><a href="mailto:Brett@communityallstars.com">Brett@communityallstars.com</a>
				</div>
			</div>
	  	</div>
  	</div>
  	
  	<?php } 
  	
  	elseif(is_page("schools")) { ?>
  	
	<div style="overflow:hidden;margin:120px auto 20px;width:960px;padding:10px;">
		
		<div class="shadow">
			<div style="width:440px;height:360px;display:inline-block;margin:0;padding:15px 15px 200px 15px;float:left;background:#1b86ff url('<?php bloginfo("template_directory"); ?>/images/sponsors-volleyball.png') no-repeat bottom center;">
				<h2 style="color:#fff;font-size:48px;text-align:center;line-height: 30px;padding:0 0 15px 0;margin-top:10px;" class="FutureBoldNormal">
				Welcome to the Team
				</h2>
				<div style="color:#fff;">
					<p class="futuramedium">CAS prides itself on customer service, school and sponsor satisfaction, and quailty of product. Through our established networks and team of professionals, we have created a one-stop, streamlined process to suit your marketing and funding needs. We do all the work and our service is 100% free to the schools. Build your brand and fund your teams! We are experts at branding and market your teams as if you were an NFL franchise. We employ only the best designers to ensure the best quality and our staff is comprised of experts who know how to effectively connect the community without intruding. We are, plain and simple, the new leader in the industry, and we work with many of the top schools in the nation including state champs and national contenders. Our products, financial returns and customer service has helped us bypass our toughest competitors. We will brand and enhance your alhletic departments "image" while raising funds and connecting the community.</p>
				</div>
			</div>
			
			<div style="padding:15px;width:460px;height:375px;display:inline-block;margin:0;float:right;background: #000;">
				<h2 style="color:#dbdbdb;font-size:24px;text-align:center;line-height: 26px;padding:0 0 15px 0;margin-top:10px;" class="futurebold">
				See what high schools nationwide are saying about us: Press Play
				</h2>
				<div style="width:379px;padding:15px 18px;height:227px;display:inline-block;margin:10px 0 0 0;float:right;background: url('<?php bloginfo("template_directory"); ?>/images/schools-tv.png') no-repeat 0 0;">
					<iframe width="337" height="189" src="https://www.youtube.com/embed/YOlSfZJ-A28?rel=0&controls=0&modestbranding=1&showinfo=0" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>
			<div style="padding:15px;width:460px;text-align:center;height:140px;color:#fff;display:inline-block;margin:0 0 0 0;float:right;background: #ffde00;">
				<img src="<? bloginfo("template_directory"); ?>/images/schools-icons.png" style="margin:15px 0 0 0;">
			</div>
		</div>
		
		<div class="shadow">
			<div style="clear:both;overflow:hidden;margin:0 auto;width:100%;padding:10px;background: url('<?php bloginfo("template_directory"); ?>/images/bg-white.png') repeat top center;">
				<div style="width:170px;display:inline-block;margin:0 10px 0 0;padding:0;float:left;">
					<h3 style="font-size:20px;margin:0 0 5px;padding:10px;display:block;background: url('<?php bloginfo("template_directory"); ?>/images/bg-h3.png') repeat top center;">
						Financial
					</h3>
					<div style="overflow:hidden;background:#1a1b20;padding:10px;height:280px;font-weight:normal;color:#fff;">
						We offer the highest financial return in the industry. A successful school will receive three rounds of funding each year. Once per season.<Br>
						<img src="<?php bloginfo("template_directory"); ?>/images/custom_schools_page_09.png" style="clear:both;margin-top:30px;">
					</div>
				</div>
				<div style="width:246px;display:inline-block;margin:0 10px 0 0;padding:0;float:left;">
					<h3 style="font-size:20px;margin:0 0 5px;padding:10px;display:block;background: url('<?php bloginfo("template_directory"); ?>/images/bg-h3.png') repeat top center;">
						Introducing CAS TV
					</h3>
					<div style="overflow:hidden;background:#1a1b20;padding:10px;height:280px;font-weight:normal;color:#fff;">
						Enhance your athletic department with a high energy, high def, professionally-made video showcasing your teams.<br>
						<img src="<?php bloginfo("template_directory"); ?>/images/custom_schools_page_06.png" style="clear:both;margin-top:70px;margin-left:12px;">
					</div>
				</div>
				<div style="width:246px;display:inline-block;margin:0 10px 0 0;padding:0;float:left;">
					<h3 style="font-size:20px;margin:0 0 5px;padding:10px;display:block;background: url('<?php bloginfo("template_directory"); ?>/images/bg-h3.png') repeat top center;">
						Introducing School Pages
					</h3>
					<div style="overflow:hidden;background:#1a1b20;padding:10px;height:280px;font-weight:normal;color:#fff;">
						We are excited to announce the launch of our new platform, School Pages. This platform changes the game and gives every school a unique page offering one streamlined process of becoming a sponsor. School Pages also puts the control back in the schools hand's allowing for live campaign monitoring as well as a platform the athletic director to call his own.
						<div style="text-align:center;margin:20px 0 0 0;padding:0;width:100%;">
							<a href="/school-pages"><img src="<?php bloginfo("template_directory"); ?>/images/button-get_started.png"></a>
						</div>
					</div>
				</div>
				<div style="width:246px;display:inline-block;margin:0;padding:0;float:left;">
					<h3 style="font-size:20px;margin:0 0 5px;padding:10px;display:block;background: url('<?php bloginfo("template_directory"); ?>/images/bg-h3.png') repeat top center;">
						School Relations
					</h3>
					<div style="overflow:hidden;background:#1a1b20;padding:10px;height:280px;font-weight:normal;color:#fff;">
						We employ only the best designers to ensure we offer the best products in the industry.<br>
						<img src="<?php bloginfo("template_directory"); ?>/images/custom_schools_page_03.png" style="clear:both;margin-top:10px;">
					</div>
				</div>
			</div>
		</div>
				
		<div class="shadow">
			<div style="width:533px;color:#fff;height:800px;display:inline-block;margin:0;padding:0px;float:left; background: #000;">
				
				<div style="padding:15px 15px 280px 15px;">
					<h2 style="color:#fff;text-align:center;font-size:40px;" class="FutureBoldNormal">Products We Deliver</h2>
					<p class="futuramedium">Our products are second to none. We employ only the best designers to ensure we continue to be the leader. All of our products are professionally designed and manufactured. Every school receives custom design so no two schools look the same.</p>
					<img src="<?php bloginfo("template_directory"); ?>/images/schools-items.png" style="margin-left:40px;margin-top:15px;">
					<div style="text-align:center;margin:20px 0 0 0;padding:0;width:100%;">
							<a href="/portfolio"><img src="<?php bloginfo("template_directory"); ?>/images/button-get_started.png"></a>
						</div>
				</div>
			</div>
			
			
			<div style="width:427px;height:800px;display:inline-block;margin:0;padding:0;float:left;color:#fff;background: #7c31ee url('<?php bloginfo("template_directory"); ?>/images/schools-bat.png') no-repeat bottom left;">
				<div style="padding:15px;margin:0;">
					
					<h2 class="FutureBoldNormal" style="color:#fff;text-align:center;font-size:42px;">Benefits</h2>
					<div class="FutureBoldNormal" style="color:#fff;font-size:20px;margin:0;padding:0 5px 0 0;">
						<ul style="margin:0 0 0 -5px;padding:0;text-transform:uppercase;line-height:140%;">
							<li>Brand your athletic department like a professional franchise.</li>
							<li>Fund your teams.</li>
							<li>Connect your local community to your school.</li>
							<li>Take your athletic dept to the next level.</li>
							<li>Our service makes you look good.</li>
							<li>3 rounds of funding per year for successful campaigns.</li>
							<li>Keepsake products for the kids and parents.</li>
							<li>Heighten school spirit.</li>
							<li>Increase attendance at games.</li>
							<li>Extremely easy process... You don't have to do much.</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="shadow">
			<div style="clear:both;overflow:hidden;margin:0 auto;width:100%;padding:10px;background: url('<?php bloginfo("template_directory"); ?>/images/bg-white.png') repeat top center;">
				<div style="width:179px;display:inline-block;margin:0 10px 0 0;padding:0;float:left;">
					<h3 style="font-size:20px;margin:0 0 5px;padding:10px;display:block;background: url('<?php bloginfo("template_directory"); ?>/images/bg-h3.png') repeat top center;">
						ADRP
					</h3>
					<div style="overflow:hidden;background:#1a1b20;padding:10px;height:230px;font-weight:normal;color:#fff;">
						The Athletic Director Referral Program is another tool you can use to gain funds for your teams and your department. It’s simple, you refer us to other athletic departments and your job is done...<br>Now all you have to do is collect the extra funds...
					</div>
				</div>
				<div style="width:416px;display:inline-block;margin:0 10px 0 0;padding:0;float:left;">
					<h3 style="font-size:20px;margin:0 0 5px;padding:10px;display:block;background: url('<?php bloginfo("template_directory"); ?>/images/bg-h3.png') repeat top center;">
						Working with Us
					</h3>
					<div style="overflow:hidden;background:#1a1b20;padding:10px;height:230px;font-weight:normal;color:#fff;">
						We work with many state champs and national contenders and would love for your school to join our team. We have truly created a win-win situation for everyone involved... We stick to our promises, fulfill our goals and represent every school with the utmost professional approach.<br>
<br>
<strong>Working with CAS is as easy as:<br>
<br>
1. SIGN UP<br>
2. SUBMIT PICS AND SCHEDULES<br>
3. RECEIVE PRODUCTS AND FUNDS</strong>
					</div>
				</div>
				<div style="width:326px;display:inline-block;margin:0;padding:0;float:left;">
					<h3 style="font-size:20px;margin:0 0 5px;padding:10px;display:block;background: url('<?php bloginfo("template_directory"); ?>/images/bg-h3.png') repeat top center;">
						More Information
					</h3>
					<div style="overflow:hidden;background:#1a1b20;padding:10px;height:230px;font-weight:normal;color:#fff;">
						For more information contact our school relations department at 866-558-1047 ext 126 or option 4 today or <br><br>
<a href="/contact">- Click here to contact us -</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	

  	<?php }
  	
  	elseif(is_page("sponsors")) { ?>
  	
	<div style="overflow:hidden;margin:160px auto 20px;width:960px;padding:10px;">
		
		<div class="shadow">
			<div style="width:440px;height:305px;display:inline-block;margin:0;padding:15px 15px 220px 15px;float:left;background:#ff5f01 url('<?php bloginfo("template_directory"); ?>/images/sponsors-basketball.png') no-repeat bottom center;">
				<h2 style="color:#fff;font-size:48px;text-align:center;line-height: 30px;padding:0 0 15px 0;margin-top:10px;" class="FutureBoldNormal">Our Team Players</h2>
				</h3>
				<div style="color:#fff;">
					<p class="futuramedium">Our sponsors/advertisers are essential to our service... The local businesses that support the teams are well rewarded with loyalty from the students, parents, staff and fans of the schools that contract us.  We offer free professional graphic design service to all sponsors.  Your business will be seen on hundreds of products distributed throughout the community and within the school.  We offer truly effective advertising with the benefit of supporting your local high schools. Your sponsorship will help pay for various necessities such as travel, uniforms and equipment for the teams. Strengthen your ties to the community and get involved! We are directly contracted by every school we work with. Welcome to the team!</p>
				</div>
			</div>
			
			<div style="padding:65px 15px 20px 24px;width:451px;height:295px;display:inline-block;margin:0;float:right;background: #c0c0c0;">
				<div id="featured" > 
					<a href="#"><img src="https://www.communityallstars.com/wp-content/uploads/2011/09/11.jpg" width="440" /></a>
					<a href="#"><img src="https://www.communityallstars.com/wp-content/uploads/2011/09/21.jpg" width="440" /></a>
					<a href="#"><img src="https://www.communityallstars.com/wp-content/uploads/2011/09/31.jpg" width="440" /></a>
				</div>
			</div>
			<div style="padding:15px;width:460px;height:130px;color:#fff;display:inline-block;margin:0;float:right;background: #4b4b4b;">
				<p class="futuramedium">"Community All-Stars has made PV Driving School known in school systems that would otherwise not know us. What good is it to be great driving instructors if no one knows you? Due to their service, our business has doubled! And on top of that, we get to help out the kids!”<br>
					- Edward H. Costello<br>
					PV Driving School, New Jersey
				</p>
			</div>
		</div>
				
		<div class="shadow">
			<div style="width:633px;color:#fff;height:800px;display:inline-block;margin:0;padding:0px;float:left; background: #000 url('<?php bloginfo("template_directory"); ?>/images/sponsors-football_team.png') no-repeat bottom center;">
				
				<div style="padding:15px 15px 280px 15px;">
					<h2 style="color:#fff000;" class="futurebold">Why CAS?</h2>
					<p class="futuramedium">Schools directly contract with us because they simply do not have the money or resources to do it themselves. We are directly contracted with high schools nationwide. We do this in bulk which is what makes it possible. We work with hundreds of high schools and are on a mission to save high school sports. We handle everything for the schools from the production to the financials. Our service and ethics make us the #1 choice for high school athletics.</p>
					<br>
					<h2 style="color:#fff000;" class="futurebold">How does it work?</h2>
					<p class="futuramedium">Becoming a sponsor for your high school is extremely easy. Every sponsor receives a sponsor packet in the mail and we handle everything for the school and we allocate the funds to them before the season starts. Our graphic design department will custom create you an ad if you would like or you can send in your own ad. We will mail you as many copies of the final products as you would like. It only takes five minutes to sign up.</p>
					<br>
					<h2 style="color:#fff000;" class="futurebold">Where will my ad be seen?</h2>
					<p class="futuramedium">Your sponsorship ad will be seen by hundreds if not thousands of people across the community and within the school. The products we create are designed to raise funds for the teams and students and will be distributed to all the students, parents, fans and staff of the school... The products will also be distributed a various community entities such as banks, doctors offices, restaurants, recreation centers etc...</p>
					<div style="text-align:center;margin:20px 0 0 0;padding:0;width:100%;">
							<a href="/school-pages"><img src="<?php bloginfo("template_directory"); ?>/images/button-get_started.png"></a>
						</div>
				</div>
			</div>
			
			
			<div style="width:327px;height:800px;display:inline-block;margin: 0;padding:0;float:left;color:#fff;background: #7C31EE url('<?php bloginfo("template_directory"); ?>/images/sponsors-football_ball.png') no-repeat bottom right;">
				<div style="padding:15px;margin:0;">
					
					<h2 style="color:#dbdbdb;font-size:20px;text-align:center;line-height: 20px;padding:0 0 0px 0;margin-top:0px;" class="futurebold">
				See what high schools nationwide are saying about us:
				</h2>
				
					<div style="width:261px;padding:15px 18px;height:160px;display:inline-block;margin:5px 0 0 0;float:right;background: url('<?php bloginfo("template_directory"); ?>/images/sponsors-sidebar-tv.png') no-repeat 0 0;">
					<iframe width="261" height="135" src="https://www.youtube.com/embed/YOlSfZJ-A28?rel=0&controls=0&modestbranding=1&showinfo=0" frameborder="0" allowfullscreen></iframe>
					</div>
					
					<h2 class="FutureBoldNormal" style="color:#fff;text-align:center;font-size:42px;">Benefits</h2>
					<div class="FutureBoldNormal" style="color:#fff;font-size:20px;margin:0;padding:0 5px 0 0;">
						<ul style="margin:0 0 0 -5px;padding:0;text-transform:uppercase;line-height: 130%;">
							<li>Connect your business to the school</li>
							<li>Help fund the teams</li>
							<li>Put your business name in the hands of students, parents, faculty & fans</li>
							<li>Some of the only advertising allowed in high schools</li>
							<li>Free graphic design</li>
							<li>PDF copy of your ad file</li>
							<li>Support the kids</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
				
		<div style="width:960px;overflow:hidden;display:inline-block;margin:0 auto;padding:0px;" class="shadow">
			<div style="padding:0px;font-weight:normal;color:#fff;overflow:hidden;font-size:15px;line-height:17px;width:960px;background: #000;">
				
				<h2 class="FutureBoldNormal" style="color:#fff;text-align:center;margin-top:15px;font-size:42px;">See our Sample Ads Below</h2>
				
				<p class="futuramedium" style="padding:0 15px;">All sponsors receive 100% free graphic design services if they choose. Our expert designers will custom create your advertisement at no charge. You may also submit your ad if you would like.<br><br></p>
				
				<div style="padding:15px;overflow:hidden;">
					<div style="width:145px;margin:0 10px 15px 4px;background:#000;float:left;display:inline-block;height:180px;">
						<a class="fancybox" href="https://www.communityallstars.com/wp-content/uploads/2011/09/1.jpg"><img width="145" src="https://www.communityallstars.com/wp-content/uploads/2011/09/1.jpg"></a>
					</div>
					<div style="width:145px;margin:0 10px 15px 0;background:#000;float:left;display:inline-block;height:180px;">
						<a class="fancybox" href="https://www.communityallstars.com/wp-content/uploads/2011/09/2.jpg"><img width="145" src="https://www.communityallstars.com/wp-content/uploads/2011/09/2.jpg"></a>
					</div>
					<div style="width:145px;margin:0 10px 15px 0;background:#000;float:left;display:inline-block;height:180px;">
						<a class="fancybox" href="https://www.communityallstars.com/wp-content/uploads/2011/09/3.jpg"><img width="145" src="https://www.communityallstars.com/wp-content/uploads/2011/09/3.jpg"></a>
					</div>
					<div style="width:145px;margin:0 10px 15px 0;background:#000;float:left;display:inline-block;height:180px;">
						<a class="fancybox" href="https://www.communityallstars.com/wp-content/uploads/2011/09/4.jpg"><img width="145" src="https://www.communityallstars.com/wp-content/uploads/2011/09/4.jpg"></a>
					</div>
					<div style="width:145px;margin:0 10px 15px 0;background:#000;float:left;display:inline-block;height:180px;">
						<a class="fancybox" href="https://www.communityallstars.com/wp-content/uploads/2011/09/5.jpg"><img width="145" src="https://www.communityallstars.com/wp-content/uploads/2011/09/5.jpg"></a>
					</div>
					<div style="width:145px;margin:0 0 15px 0;background:#000;float:left;display:inline-block;height:180px;">
						<a class="fancybox" href="https://www.communityallstars.com/wp-content/uploads/2011/09/6.jpg"><img width="145" src="https://www.communityallstars.com/wp-content/uploads/2011/09/6.jpg"></a>
					</div>
				</div>
				
				<div style="width:100%;clear:both;height:1px;margin:0;padding:0;border:0;"></div>
				
				<div style="width:450px;float:left;text-align:center;display:inline-block;margin:0 0 0 0;padding:15px;">
					<img src="<?php bloginfo("template_directory"); ?>/images/sponsors_ad_samples.png" style="margin-top:5px;">
					<h2 class="FutureBoldNormal" style="font-size:30px;color:#fff;margin-top:10px;">Get Started Now<br>
					<div style="text-align:center;margin:5px 0 0 0;padding:0;width:100%;">
							<a href="/school-pages"><img src="<?php bloginfo("template_directory"); ?>/images/button-get_started.png"></a>
						</div>
					Or Call Our<Br>Sponsor Line<br>
					<span style="color:#ffff00;">866-558-1042</span></h2>
				</div>
				<div style="width:450px;height:300px;float:right;text-align:center;display:inline-block;margin:0 0 0 0;padding:15px;background:#c5fcff;">
					<h2 class="FutureBoldNormal" style="color:#6c7b7c;font-size:40px;margin-top:20px;">To Become a Sponsor</h2>
    				<div id="home_search" style="text-align:center;margin:20px auto 0;height:160px;">
						<img src="<?php bloginfo("template_directory"); ?>/images/home/home_search_header.png">
						<form method="post" action="https://www.communityallstars.com">
							<input type="text" id="home_search_input" name="s" />
							<input type="submit" id="home_search_submit" />
						</form>
    				</div>
				</div>
			</div>
		</div>
	</div>

  	<?php }
  	
  	elseif(is_page("portfolio")) { ?>
  	
	<div style="overflow:hidden;margin:80px auto 20px;width:960px;padding:10px;">
		
		<div class="shadow">
			<div style="width:440px;height:445px;display:inline-block;margin:0;padding:15px 15px 200px 15px;float:left;background:#ff5f01 url('https://www.communityallstars.com/wp-content/uploads/2011/09/sponsors_the_best_products1.jpg') no-repeat bottom center;">
				<h2 style="color:#fff;font-size:48px;text-align:center;line-height: 30px;padding:0 0 15px 0;margin-top:10px;" class="FutureBoldNormal">
					The Best Products
				</h2>
				<div style="color:#fff;">
					<p class="futuramedium">We employ only the best designers to ensure we deliver the best products. All of our products are professionally designed and manufactured in the USA. Our cutting edge design process has been a major help in maintaining our leadership role in the industry. Every product we create is custom designed.</p>
				</div>
			</div>
			
			<div style="padding:0;width:490px;height:660px;display:inline-block;margin:0;float:right;background: #c0c0c0;">
				<div id="portfolio1" style="width:490px;height:660px;" > 
					<a class="fancybox" href="<?php bloginfo("template_directory"); ?>/images/posters/1.jpg"><img src="<?php bloginfo("template_directory"); ?>/images/posters/1.jpg"></a>
					<a class="fancybox" href="<?php bloginfo("template_directory"); ?>/images/posters/2.jpg"><img src="<?php bloginfo("template_directory"); ?>/images/posters/2.jpg"></a>
					<a class="fancybox" href="<?php bloginfo("template_directory"); ?>/images/posters/3.jpg"><img src="<?php bloginfo("template_directory"); ?>/images/posters/3.jpg"></a>
					<a class="fancybox" href="<?php bloginfo("template_directory"); ?>/images/posters/4.jpg"><img src="<?php bloginfo("template_directory"); ?>/images/posters/4.jpg"></a>
					<a class="fancybox" href="<?php bloginfo("template_directory"); ?>/images/posters/5.jpg"><img src="<?php bloginfo("template_directory"); ?>/images/posters/5.jpg"></a>
					<a class="fancybox" href="<?php bloginfo("template_directory"); ?>/images/posters/6.jpg"><img src="<?php bloginfo("template_directory"); ?>/images/posters/6.jpg"></a>
					<a class="fancybox" href="<?php bloginfo("template_directory"); ?>/images/posters/7.jpg"><img src="<?php bloginfo("template_directory"); ?>/images/posters/7.jpg"></a>
				</div>
			</div>
		</div>
				
		<div class="shadow">

			<div style="width:960px;overflow:hidden;display:inline-block;margin:0 auto;padding:0px;" class="shadow">
				<div style="padding:0px;font-weight:normal;color:#fff;overflow:hidden;font-size:15px;line-height:17px;width:960px;background: #000;">
					
					<h2 class="FutureBoldNormal" style="float:left;width:250px;color:#fff;margin: 15px 0 15px 15px; display:inline-block;font-size:35px;">Action Posters:</h2>
					
					<p class="futuramedium" style="float:left;width:670px;padding:15px 0 0;display:inline-block;">Our posters are by far our most popular and in demand product. They feature action pictures of the student athletes, team schedules, school components and community sponsors.<br><br></p>
					
					<div style="clear:both;width:960px;height:1px;margin:0;padding:0;border:none;"></div>
					<div class="container" style="width:960px;">
						<div class="wt-scroller" style="width:960px;">
							<div class="prev-btn"></div>          
							<div class="slides" style="width:890px;">
							    <ul>
							        <li>
							            <a class="fancybox" href="<?php bloginfo("template_directory"); ?>/images/horizontal/horizontal1.jpg" rel="scroller"><img src="<?php bloginfo("template_directory"); ?>/images/horizontal/horizontal1.jpg" alt=""/></a>    
							        </li>
							        <li>                        	
							            <a class="fancybox" href="<?php bloginfo("template_directory"); ?>/images/horizontal/horizontal2.jpg" rel="scroller"><img src="<?php bloginfo("template_directory"); ?>/images/horizontal/horizontal2.jpg" alt=""/></a>
							        </li>
							        <li>
							            <a class="fancybox" href="<?php bloginfo("template_directory"); ?>/images/horizontal/horizontal3.jpg" rel="scroller"><img src="<?php bloginfo("template_directory"); ?>/images/horizontal/horizontal3.jpg" alt=""/></a>
							        </li>
							        <li>                    
							            <a class="fancybox" href="<?php bloginfo("template_directory"); ?>/images/horizontal/horizontal1.jpg" rel="scroller"><img src="<?php bloginfo("template_directory"); ?>/images/horizontal/horizontal1.jpg" alt=""/></a>
							        </li>              
							        <li>
							            <a class="fancybox" href="<?php bloginfo("template_directory"); ?>/images/horizontal/horizontal2.jpg" rel="scroller"><img src="<?php bloginfo("template_directory"); ?>/images/horizontal/horizontal2.jpg" alt=""/></a>
							        </li>
							        <li>
							            <a class="fancybox" href="<?php bloginfo("template_directory"); ?>/images/horizontal/horizontal3.jpg" rel="scroller"><img src="<?php bloginfo("template_directory"); ?>/images/horizontal/horizontal3.jpg" alt=""/></a>                                              
								    </li>

							    </ul>
							</div>          	
							<div class="next-btn"></div>
							<div class="lower-panel"></div>
						</div>
					</div>

					
					
				</div>
			</div>
		</div>
			
		<div class="shadow">
			<div style="border:10px solid #fff;width:940px;color:#fff;overflow:hidden;display:inline-block;margin:0;padding:0px;float:left;background:#000;">
				<div style="width:910px;background:#333;padding:10px;">
					<div style="width:33%;display:inline-block;">
						<h3 class="FutureBoldNormal" style="color:#fff;padding:0;margin:0;">Programs</h3>
					</div>
					<div style="width:33%;display:inline-block;">
						<h3 class="FutureBoldNormal"  style="color:#fff;padding:0;margin:0;">Schedule Cards</h3>
					</div>
					<div style="width:33%;display:inline-block;">
						<h3 class="FutureBoldNormal"  style="color:#fff;padding:0;margin:0;">Apparel</h3>
					</div>
				</div>
				<div style="width:910px;padding:15px;">
					<div style="width:33%;display:inline-block;vertical-align:top;">
						<p>A magazine style publicztion featuring school info and articles team schedules action pictures of the students, stats,leeters from the staff,athlete bio’s and sponsor ads.</p>
						<div style="width:290px;height:200px; background:#333;">
							<div id="portfolio2" style="width:290px;height:200px;" > 
								<a class="fancybox" href="<?php bloginfo("template_directory"); ?>/images/programs/1.jpg"><img src="<?php bloginfo("template_directory"); ?>/images/programs/1.jpg" width="290"></a>
								<a class="fancybox" href="<?php bloginfo("template_directory"); ?>/images/programs/2.jpg"><img src="<?php bloginfo("template_directory"); ?>/images/programs/2.jpg" width="290"></a>
								<a class="fancybox" href="<?php bloginfo("template_directory"); ?>/images/programs/3.jpg"><img src="<?php bloginfo("template_directory"); ?>/images/programs/3.jpg" width="290"></a>
							</div>
						</div>
					</div>
					<div style="width:33%;display:inline-block;vertical-align:top;">
						<p>A bi-fold wallet size atheltic calendar featuring an action photo of a student athlete, game schedules and a sponsor ad.</p><p style="text-align:center;"><a href="<?php bloginfo("template_directory"); ?>/images/sponsor_card.jpg" class="fancybox"><img src="<?php bloginfo("template_directory"); ?>/images/portfolio-schedule_cards.png"></a></p>
					</div>
					<div style="width:33%;display:inline-block;vertical-align:top;">
						<p>Custom hight quality apparel featuring school colors, logos and sponsors.</p>
						<p style="text-align:center;"><img src="<?php bloginfo("template_directory"); ?>/images/portfolio-apparel.png"></p>
					</div>
				</div>
				<div style="width:910px;background:#333;padding:10px;">
					<div style="width:49%;display:inline-block;">
						<h3  class="FutureBoldNormal" style="color:#fff;padding:0;margin:0;">CAS TV</h3>
					</div>
					<div style="width:50%;display:inline-block;">
						<h3 class="FutureBoldNormal"  style="color:#fff;padding:0;margin:0;">Custom Production</h3>
					</div>
				</div>
				<div style="width:910px;padding:15px;">
					<div style="width:49%;display:inline-block;vertical-align:top;">
						<p>Enhance yours athletics department with a hight energy, high def, professionally made video / commercial showcasing yours teams. These videos can be uesd across a variety of platforms including social media, yours website, pep-rallys and even the local news channel.</p><p style="text-align:center;"><img src="<?php bloginfo("template_directory"); ?>/images/portfolio-cas_tv.png"></p>
					</div>
					<div style="width:50%;display:inline-block;vertical-align:top;">
						<p>We can custom produce and manufacture special products at the request of the schools. If you would like to request custom production do not hesitate to contact our design team. Custom production may include banners, spirit items, water bottles etc...</p>
					</div>
				</div>
			</div>
		</div>
	

	</div>

  	<?php }
  	
  	else { ?>
  	<div class="grid_16">
    	<div class="bloglist-panel">
      		<div class="blog-post">
       		 <p style="margin-top:-14px;"><?php the_content(); ?></p>
      		</div>
    	</div>    
  	</div>
  	<?php } ?>
  	
  
</div>
<!-- Blog and Siderbar Start -->
<div style="clear:both;"></div>
<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>