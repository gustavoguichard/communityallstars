<?php
/**
 * The template for displaying Search Results pages.
 * * @package WordPress
 * @subpackage community-all-stars
 * @since community-all-stars 1.0
 */

get_header(); ?>
  <div class="main-content section">
    <div class="w-container">
      <div class="w-row main-row">
        <h3 class="about-heading"><?php printf( __( 'Search Results for: %s', '' ), get_search_query() ); ?></h3>
        <?php if (have_posts()) : ?>
          <?php while (have_posts()) : the_post(); ?>
            <div class="bloglist-panel" style="margin-right:15px;">
              <div class="bloglist-title">
                <h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
              </div>
              <div class="bloglist-post">
                <a href="<?php the_permalink() ?>" class="button home-button">View This <?php if(get_post_type() == "schools") { echo "School"; } else { echo "Page"; } ?></a>
                <div style=" margin-top:26px;"></div>
              </div>
            </div>
          <?php endwhile; ?>
          <div class="page-navi">
            <?php posts_nav_link( ' ', '<img src="' . get_bloginfo('stylesheet_directory') . '/images/b2.png" />', '<img src="' . get_bloginfo('stylesheet_directory') . '/images/b1.png" />' ); ?>
          </div>
        <?php else : ?>
          <h2 style="text-align:center;"><?php _e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', '' ); ?></h2>
        <?php endif; ?>
      </div>
    </div>
  </div>
<?php get_footer(); ?>
