<?php
/**
 * The Template for displaying all single posts.
 * * @package WordPress
 * @subpackage community-all-stars
 * @since community-all-stars 1.0
 */

get_header();

$test = false; //// <<------------------------------- Set to true for testing

// Process Form & Payment
if($_POST['submit']) {

	// General
	date_default_timezone_set('America/Los_Angeles');
	
	// While Developing
	ini_set("display_errors",0);
	ini_set("error_reporting",-1);
	
	// Gloabl Var	
	$wp_content = "/home/content/98/8417598/html/wp-content";
	$upload_location = $wp_content."/uploads/school_files/";
	$httpdocs_folder = "html";
	$the_domain = "https://www.communityallstars.com";
		
	if(!$_POST['invoice']) {
		// Uploaded File Handling
		ini_set("max_execution_time",600);				// 10 minutes
		ini_set("max_input_time",600);					// 10 minutes
		$max_filesize = 100 * 1024 * 1024; 				// 100 MB Per file
		ini_set("upload_max_filesize","300M");			// 300 MB total (Form allows 3 files)
		ini_set("post_max_size","300M");
		$supported_formats = array("image/gif","image/jpg","image/jpeg","image/pjpeg","image/png","image/x-png","image/tif","image/tiff","image/x-tif","image/x-tiff","application/tif","application/tiff","application/x-tif","application/x-tiff","image/bmp","image/ms-bmp","image/x-bitmap","image/x-bmp","image/x-ms-bmp","image/x-win-bitmap","image/x-windows-bmp","image/x-xbitmap","application/bmp","application/x-bmp","image/photoshop","image/x-photoshop","image/psd","image/x-psd","application/photoshop","application/x-photoshop","application/psd","application/x-psd","image/eps",	"image/x-eps","application/postscript","application/eps","application/x-eps","application/illustrator");
		
		// Calculations using post data
		$season_data = explode(" - $",$_POST['seasonSelect']);
		$season_price = str_replace(",","",$season_data[1]);
		$seasons_purchased = $season_data[0];
		/**/
		$ad_size_array = explode("(",stripslashes($_POST['size']));
		$ad_size = $ad_size_array[0];
	}
	
	// Create Email Message
	$i = 1;		// For the "File #" lines within the message 
	$email_header  = 'MIME-Version: 1.0' . "\r\n";
	$email_header .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	$email_header .= 'From: CAS Orders <orders@communityallstars.com>' . "\r\n";
	$email_header .= 'Cc: orders@communityallstars.com' . "\r\n";
	if($_POST['invoice']) {
		$the__subject = "An invoice has been paid!";
		$message = "An invoice has been paid!<br><br>";
		$message .= "<strong>Invoice #:</strong> ". $_POST['invoice']."<br>";
	}	
	else {
		$the__subject = "New School Submitted";
		$message = "A order has been placed for a new ad!<br><br>";
	}
	$message .= "<strong>School:</strong> ". get_the_title()."<br>";
	$message .= "<strong>Contact:</strong> ". $_POST['first_name']." ".$_POST['last_name']."<br>";
	$message .= "<strong>Email:</strong> ". $_POST['email']."<br>";
	$message .= "<strong>Phone:</strong> (". $_POST['phone_area'].") ". $_POST['phone_number']."<br>";
	if(!$_POST['invoice']) {
		$message .= "<strong>Business:</strong> (". $_POST['fax_area'].") ". $_POST['fax_number']."<br>";
		$message .= "<strong>Website:</strong> ". $_POST['website']."<br>";	
	}
	$message .= "<strong>Organization:</strong> ". $_POST['organization']."<br>";
	$message .= "<strong>Billing Name:</strong> ". $_POST['bill_first_name']." ".$_POST['bill_last_name']."<br>";
	$message .= "<strong>Billing Address:</strong> ". $_POST['billing_address_1']."<br>";
	$message .= "<strong>Billing City:</strong> ". $_POST['billing_city']."<br>";
	$message .= "<strong>Billing State:</strong> ". $_POST['billing_state']."<br>";
	$message .= "<strong>Billing Zip:</strong> ". $_POST['billing_zip']."<br>";
	if(!$_POST['invoice']) {
		$message .= "<strong>Helped by Rep:</strong> ". $_POST['business_rep']."<br>";
		$message .= "<strong>Ad Size:</strong> ".$ad_size."<br>";
		$message .= "<strong>Seasons Purchased:</strong> ".$seasons_purchased."<br>";
		$message .= "<strong>Price:</strong> ".$season_price."<br>";
	}

	if(!$_POST['invoice']) {
		// Process Files
		foreach($_FILES as $file) {
			if(!empty($file['name'])) {
				
				// File Handling - Individual
				$safe_filename = preg_replace( array("/\s+/", "/[^-\.\w]+/"), array("_", ""), trim($file['name']));
				$time_stamp = date("Y-m-d-H-i-s-u");
				$file_location = $upload_location . $time_stamp ."-".$safe_filename;
				
				// Process Files			 
				if (in_array( $file["type"],$supported_formats)) {
					if (file_exists($file_location)) {
						$alerts[] = "Error! ".$safe_filename . " already exists on our server.";
					}
					else {
						if($file['size'] < $max_filesize) {
							$move = move_uploaded_file($file["tmp_name"], $file_location );
							if($move) {
								$url = explode($httpdocs_folder,$file_location);
								$notify = "<strong>Ad file #".$i." downloadable at:</strong> ". $the_domain. $url[1]."<br>";
								$message .= $notify;
								$i++;
							}
							else {
								$alerts[] = "Error! Your file could not be stored. Please try again.";
							}
						}
						else {
							$alerts[] = "Error! File size too large. Please upload a file that is less than ".$max_filesize."MB";
						}
					}
				}
				else {
				  $alerts[] = "Error! Invalid file type.";
				}
			}
		}
	}
	
	// Process Transaction if no file issues
	if(empty($alerts) || $test == true || !empty($_POST['invoice'])) {
	
		// Process Authorize.net Transaction
		require_once $wp_content."/themes/community-all-stars/authorize/AuthorizeNet.php";
		
		$sale = new AuthorizeNetAIM("6J2faV84","2PY9Ybtsx42X37Tu");
	
		// Credentials
		$sale->setSandbox(false);
		$sale->test_request = $test;
		
		// Billing info
		$sale->first_name = $_POST['bill_first_name'];
		$sale->last_name = $_POST['bill_last_name'];
		$sale->address = $_POST['billing_address'];
		$sale->city = $_POST['billing_city'];
		$sale->state = $_POST['billing_state'];
		$sale->zip = $_POST['billing_zip'];
		
		// The Sale
		if(!empty($_POST['invoice'])) {
			$charge_amount = $_POST['amount'];
		}
		else if($test) {
			$charge_amount = 1;
		}
		else {
			$charge_amount = $season_price;
		}
		
		$sale->amount = $charge_amount;
		$sale->card_num = $_POST['cc_card'];
		$sale->exp_date = $_POST['cc_month']."/".$_POST['cc_year'];
		
		$response = $sale->authorizeAndCapture();
		
		if ($response->approved || $test == true) {
			$alerts[] = "Success! Your credit card has been charged and your order has been processed!";
			$alerts[] = "Transaction ID: ". $response->transaction_id;
			
			// Add Authorize details to message & then mail
			$message .= "<strong>Amount Charged</strong> ".$response->amount."<br>";
			$message .= "<strong>Card Used:</strong> ".$response->account_number."<br>";
			$message .= "<br><strong>Authorize Transaction ID:</strong> ".$response->transaction_id."<br>";			
			$mail = mail("lauren@communityallstars.com,brett@communityallstars.com,mike@communityallstars.com", $the__subject, $message, $email_header);$mail = mail("lauren@communityallstars.com,brett@communityallstars.com,mike@communityallstars.com", "New School Submitted", $message, $email_header);
			//$mail = mail("justin@justindocanto.com", $the__subject, $message, $email_header);
		}
		else {
			$alerts[] = "There was an error with your transaction";
			// $alerts[] = $response->response_reason_code;
		 	$alerts[] = $response->response_reason_text;
		}
	}
}

// Grab School Data for page
$custom_fields = get_post_custom(get_the_ID());
foreach ( $custom_fields as $key => $value ) {
	$schools_meta[$key] = $value[0];
}

?>
<script type="text/javascript">
	function showsize(the_type) {
		var the_size = document.getElementById("choose-size");
		the_size.style.display = "block";
		var business_list = document.getElementById("size0");
		if(the_type == 1) {
			business_list.disabled = true;
		}
		if(the_type == 0) {
			business_list.disabled = false;
		}
	}
	function showContact() {
		var the_contact = document.getElementById("choose-contact");
		the_contact.style.display = "block";
		var upload_files = document.getElementById("upload_files");
		upload_files.style.display = "block";
	}
	function showContactandFiles() {
		var upload_files = document.getElementById("upload_files");
		upload_files.style.display = "block";
		var the_contact = document.getElementById("choose-contact");
		the_contact.style.display = "block";
	}
	function showDesign() {
		var the_design = document.getElementById("choose-design");
		the_design.style.display = "block";
	}
	function addToSelect(size) {
		// Show Div
		// var the_seasons = document.getElementById("choose-seasons");
		// the_seasons.style.display = "block";
				
		// Clear options
		document.getElementById('seasonSelect').length = 0
		
		// Populate options based on size input
		var SelectField = document.getElementById("seasonSelect");
		var CreateOption0 = document.createElement("OPTION");			// Select One									
		var CreateOption1 = document.createElement("OPTION");			// 1 Year
		var CreateOption2 = document.createElement("OPTION");			// 3 Years	
		SelectField.options.add(CreateOption0);
		SelectField.options.add(CreateOption1);
		SelectField.options.add(CreateOption2);
		CreateOption0.text = "Select One";	
		CreateOption1.name = "seasonSelect";
		CreateOption2.name = "seasonSelect";
		if(size == "0") {
			CreateOption1.text = "1 Season - $125";
			CreateOption1.value = "1 Season - $125";
			CreateOption2.text = "3 Seasons - $300";
			CreateOption2.value = "3 Seasons - $300";
		}
		else if(size == "1") {
			CreateOption1.text = "1 Season - $245";
			CreateOption1.value = "1 Season - $245";
			CreateOption2.text = "3 Seasons - $600";
			CreateOption2.value = "3 Seasons - $600";
		}
		else if(size == "2") {
			CreateOption1.text = "1 Season - $375";
			CreateOption1.value = "1 Season - $375";
			CreateOption2.text = "3 Seasons - $975";
			CreateOption2.value = "3 Seasons - $975";
		}
		else if(size == "3") {
			CreateOption1.text = "1 Season - $445";
			CreateOption1.value = "1 Season - $445";
			CreateOption2.text = "3 Seasons - $1,100";
			CreateOption2.value = "3 Seasons - $1,100";
		}
		else if(size == "4") {
			CreateOption1.text = "1 Season - $645";
			CreateOption1.value = "1 Season - $645";
			CreateOption2.text = "3 Seasons - $1,700";
			CreateOption2.value = "3 Seasons - $1,700";
		}
		else if(size == "5") {
			CreateOption1.text = "1 Season - $845";
			CreateOption1.value = "1 Season - $845";
			CreateOption2.text = "3 Seasons - $2,300";
			CreateOption2.value = "3 Seasons - $2,300";
		}
		else if(size == "6") {
			CreateOption1.text = "1 Season - $1,275";
			CreateOption1.value = "1 Season - $1,275";
			CreateOption2.text = "3 Seasons - $3,200";
			CreateOption2.value = "3 Seasons - $3,200";
		}
		else if(size == "7") {
			CreateOption1.text = "1 Season - $1,575";
			CreateOption1.value = "1 Season - $1,575";
			CreateOption2.text = "3 Seasons - $3,900";
			CreateOption2.value = "3 Seasons - $3,900";
		}
		else if(size == "8") {
			CreateOption1.text = "1/16 - 1 Season - $245";
			CreateOption1.value = "1/16 - 1 Season - $245";
			CreateOption2.text = "1/16 - 3 Seasons - $600";
			CreateOption2.value = "1/16 - 3 Seasons - $600";
		}
		else if(size == "9") {
			CreateOption1.text = "1/8 - 1 Season - $375";
			CreateOption1.value = "1/8 - 1 Season - $375";
			CreateOption2.text = "1/8 - 3 Seasons - $975";
			CreateOption2.value = "1/8 - 3 Seasons - $975";
		}
		else if(size == "10") {
			CreateOption1.text = "1/4 - 1 Season - $445";
			CreateOption1.value = "1/4 - 1 Season - $445";
			CreateOption2.text = "1/4 - 3 Seasons - $1,100";
			CreateOption2.value = "1/4 - 3 Seasons - $1,100";
		}
		else if(size == "11") {
			CreateOption1.text = "1/2 - 1 Season - $645";
			CreateOption1.value = "1/2 - 1 Season - $645";
			CreateOption2.text = "1/2 - 3 Seasons - $1,700";
			CreateOption2.value = "1/2 - 3 Seasons - $1,700";
		}
		else if(size == "12") {
			CreateOption1.text = "Full Page - 1 Season - $845";
			CreateOption1.value = "Full Page - 1 Season - $845";
			CreateOption2.text = "Full Page - 3 Seasons - $2,300";
			CreateOption2.value = "Full Page - 3 Seasons - $2,300";
		}
		else if(size == "13") {
			CreateOption1.text = "Inside/Back Cover - 1 Season - $1,275";
			CreateOption1.value = "Inside/Back Cover - 1 Season - $1,275";
			CreateOption2.text = "Inside/Back Cover - 3 Seasons - $3,200";
			CreateOption2.value = "Inside/Back Cover - 3 Seasons - $3,200";
		}
		else if(size == "14") {
			CreateOption1.text = "Double Truck - 1 Season - $1,575";
			CreateOption1.value = "Double Truck - 1 Season - $1,575";
			CreateOption2.text = "Double Truck - 3 Seasons - $3,900";
			CreateOption2.value = "Double Truck - 3 Seasons - $3,900";
		}
		else if(size == "15") {
			CreateOption1.text = "Centerfold - 1 Season - $1,775";
			CreateOption1.value = "Centerfold - 1 Season - $1,775";
			CreateOption2.text = "Centerfold - 3 Seasons - $4,500";
			CreateOption2.value = "Centerfold - 3 Seasons - $4,500";
		}
	}
	jQuery(document).ready(function() {
		$ = jQuery;
		var smoothScroll;
		smoothScroll = function(anchor) {
		  var offsetToGo;
		  offsetToGo = anchor != null ? jQuery(anchor).offset().top : 0;
		  return jQuery('html, body').clearQueue().animate({
		    scrollTop: offsetToGo + 'px'
		  }, {
		    duration: 350
		  });
		};
		$("#pay-button").click(function(){
			$(this).css("text-decoration","none");
			$(this).html("<h3>Pay it Now</h3>");
		 	$("#pay-form").fadeIn();
		 });
		$('#choose-size input').on('change', function(){
			$('#choose-seasons').show();
			smoothScroll('#choose-seasons');
		})
	});
</script>
<div class="main-content section">
  <div class="w-container">
    <div class="w-row">
    	<div class="w-col w-col-3">
				<div class="school-box" style="text-align:center;">
					<?php if($schools_meta['schools_page_link']):?>
						<a href="<?php echo $schools_meta['schools_page_link'];?>" target="_blank">
							<img src="<?php echo $schools_meta['schools_flyer']; ?>" id="school_logo">
						</a>
					<?php else:?>
						<img src="<?php echo $schools_meta['schools_flyer']; ?>" id="school_logo">
					<?php endif;?>
				</div>
    	</div>
    	<div class="w-col w-col-9">
				<div class="school-box">
					<h1><?php the_title(); ?></h1>
					<h2><?php echo $schools_meta['schools_headliner']; ?></h2>
					<hr/>
					<p style="margin-bottom:0;font-size:10px"><?php echo $schools_meta['schools_description']; ?></p>
					<p style="padding-top:4px;"><strong style="color:#00afe9;font-size:14px;"><?php echo $schools_meta['schools_supporting']; ?></strong></p>
				</div>
			</div>
		</div>
		<div class="w-row">
			<div class="w-col w-col-4">
				<div class="school-title-box">
					<h3>Last Season's Poster</h3>
				</div>
				<div class="school-box">
					<a href="#school_modal" rel="leanModal"><img src="<?php echo $schools_meta['schools_slider_img']; ?>" id="school_flyer"></a>
				</div>
				<?php if($schools_meta['schools_testimonial_author']):?>
					<div class="school-title-box">
						<h3>Hearsay</h3>
					</div>
					<div class="school-box">
						<p>
							“<?php echo $schools_meta['schools_testimonial']; ?>”<br><br>
							<strong>- <?php echo $schools_meta['schools_testimonial_author']; ?></strong>
						</p>
					</div>
				<?php endif;?>
			</div>
			<div class="w-col w-col-5">
				<div class="school-title-box">
					<h3>Become a Sponsor</h3>
				</div>
				<div class="school-box">
					<?php if(!empty($alerts)) { ?>
						<?php foreach($alerts as $key=>$value) { ?>
							<h3 style="color:#ff0000;"><?php echo $value."<br>"; ?></h3>
						<?php } ?>
					<?php } ?>
					<div class="form-labels">
						<p class="question">Becoming a sponsor is very easy and only takes 2 minutes.</p>
						<p>Please note, All sponsors receive 100% free custom graphic design service for their advertisement if they choose.</p>
					</div>
					<form method="post" action="#" enctype="multipart/form-data" >
						<div id="choose-type" class="form-labels">
							<p class="question">Are you a Business or Individual/Family?</p>
							<div><input id="type1" type="radio" class="radio" name="type" value="Business" onClick="showsize(0);" /><label for="type1">Business</label></div>
							<div><input id="type2" type="radio" class="radio" name="type" value="Individual/Family" onClick="showsize(1);" /><label for="type2">Individual/Family</label></div>
						</div>
						<div id="choose-size" class="form-labels">
							<?php if($schools_meta['schools_poster_type'] != 2):?>
							 <p class="question">Please choose the ad size</p>
							 <hr/>
							 <p><strong>Poster:</strong></p>
							 <div class="blue_box_wrapper"><input id="size0" onClick="addToSelect(0);" type="radio" class="radio" name="size" value="Text Only ($125)"><label for="size0">Business Listing<br>$125<br><i>Text Only</i></label></div>
							 <div class="blue_box_wrapper"><input id="size1" onClick="addToSelect(1);" type="radio" class="radio" name="size" value="2'' x 2'' ($245)"><label for="size1">2" x 2"<br>$245<br><div class="blue_box" style="width:30px;height:30px;"></div></label></div>
							 <div class="blue_box_wrapper"><input id="size2" onClick="addToSelect(2);" type="radio" class="radio" name="size" value="3'' x 2'' ($375)"><label for="size2">3" x 2"<br>$375<br><div class="blue_box" style="width:45px;height:30px;"></div></label></div>
							 <div class="blue_box_wrapper"><input id="size3" onClick="addToSelect(3);" type="radio" class="radio" name="size" value="4'' x 2'' ($445)"><label for="size3">4" x 2"<br>$445<br><div class="blue_box" style="width:60px;height:30px;"></div></label></div>
							 <div class="blue_box_wrapper"><input id="size4" onClick="addToSelect(4);" type="radio" class="radio" name="size" value="4'' x 4'' ($645)"><label for="size4">4" x 4"<br>$645<br><div class="blue_box" style="width:60px;height:60px;"></div></label></div>
							 <div class="blue_box_wrapper"><input id="size5" onClick="addToSelect(5);" type="radio" class="radio" name="size" value="6'' x 4'' ($845)"><label for="size5">6" x 4"<br>$845<br><div class="blue_box" style="width:90px;height:60px;"></div></label></div>
							 <div class="blue_box_wrapper"><input id="size6" onClick="addToSelect(6);" type="radio" class="radio" name="size" value="18'' x 2'' ($1275)"><label for="size6">18" x 2"<br>$1,275<br><div class="blue_box" style="width:270px;height:30px;"></div></label></div>
							 <div class="blue_box_wrapper"><input id="size7" onClick="addToSelect(7);" type="radio" class="radio" name="size" value="23'' x 2'' ($1575)"><label for="size7">23" x 2"<br>$1,575<br><div class="blue_box" style="width:299px;height:26px;"></div></label></div>
							<?php else:?>
							 <hr/>
							 <p><strong>Program:</strong></p>
							 <div class="blue_box_wrapper"><input id="size0" onClick="addToSelect(0);" type="radio" class="radio" name="size" value="Text Only ($125)"><label for="size0">Business Listing<br>$125<br><i>Text Only</i></label></div>
							 <div class="blue_box_wrapper"><input id="size8" onClick="addToSelect(8);" type="radio" class="radio" name="size" value="1/16 Page ($245)"><label for="size8">1/16 Page<br>$245<br><div class="blue_box" style="width:20px;height:28px;"></div></label></div>
							 <div class="blue_box_wrapper"><input id="size9" onClick="addToSelect(9);" type="radio" class="radio" name="size" value="1/8 Page ($375)"><label for="size9">1/8 Page<br>$375<br><div class="blue_box" style="width:40px;height:28px;"></div></label></div>
							 <div class="blue_box_wrapper"><input id="size10" onClick="addToSelect(10);" type="radio" class="radio" name="size" value="1/4 Page ($445)"><label for="size10">1/4 Page<br>$445<br><div class="blue_box" style="width:40px;height:56px;"></div></label></div>
							 <div class="blue_box_wrapper"><input id="size11" onClick="addToSelect(11);" type="radio" class="radio" name="size" value="1/2 Page ($645)"><label for="size11">1/2 Page<br>$645<br><div class="blue_box" style="width:80px;height:56px;"></div></label></div>
							 <div class="blue_box_wrapper"><input id="size12" onClick="addToSelect(12);" type="radio" class="radio" name="size" value="Full Page ($845)"><label for="size12">Full Page<br>$845<br><div class="blue_box" style="width:80px;height:112px;"></div></label></div>
							 <div class="blue_box_wrapper"><input id="size13" onClick="addToSelect(13);" type="radio" class="radio" name="size" value="Inside/Back Cover ($1275)"><label for="size13">Inside/Back Cover<br>$1,275<br><div class="blue_box" style="width:80px;height:112px;"></div></label></div>
							 <div class="blue_box_wrapper"><input id="size14" onClick="addToSelect(14);" type="radio" class="radio" name="size" value="Double Truck (2 pgs) ($1575)"><label for="size14">Double Truck (2 pgs)<br>$1,575<br><div class="blue_box" style="width:160px;height:112px;"></div></label></div>
							 <div class="blue_box_wrapper"><input id="size15" onClick="addToSelect(15);" type="radio" class="radio" name="size" value="Centerfold (2 pgs) ($1775)"><label for="size15">Centerfold (2 pgs)<br>$1,775<br><div class="blue_box" style="width:160px;height:112px;"></div></label></div>
							<?php endif;?>
						</div>	
						<div id="choose-seasons" class="form-labels">
							 <p class="question" style="padding-bottom:0;margin-bottom:0;">Please choose how many seasons you would like to place your ad for</p>
							 <i>Full year sponsorships receive discount</i><br>
							 <select id="seasonSelect" onChange="showDesign();" name="seasonSelect">
							 </select>
						</div>
						
						<div id="choose-design" class="form-labels">
							 <p class="question" style="padding-bottom:0;margin-bottom:0;">Would you like us to design your ad for free?<br></p><i>If you choose yes, our design team will contact you shortly to begin creating your ad at no extra charge</i>
							 
							 <div style="clear:both;height:1px;width: 100%;margin:0;padding:0;"></div>
							 
							 <input onClick="showContact();" type="radio" class="radio" name="design" id="design1" value="Yes"><label for="design1">Yes</label>
							 <input onClick="showContactandFiles();" type="radio" class="radio" name="design" id="design2" value="No"><label for="design2">No</label>
							 
							<div id="upload_files">
								<p class="question">If you have your own artwork, please upload your ad file(s)</p>
								<input type="file" name="file1">
								<input type="file" name="file2">
								<input type="file" name="file3">
							</div>
						</div>
						
						<div id="choose-contact">
							<div class="form-labels">
								<p class="question">Please enter your Contact Information</p>
								<label class="contact_label" for="organization">Organization Name</label>
									<input type="text" name="organization" class="long" />
								<label class="contact_label" for="first_name">Full Name</label>
									<input type="text" name="first_name" class="medium" />
									<input type="text" name="last_name" class="medium" />
								<label class="contact_label" for="email">E-Mail</label>
									<input type="text" name="email" class="regular" />
								<label class="contact_label" for="website">Website</label>
									<input type="text" name="website" class="regular" />
								<label class="contact_label" for="phone_area">Contact Phone</label>
									<input type="text" name="phone_area" class="mini" />
									<input type="text" name="phone_number" class="medium" />
								<label class="contact_label" for="fax_area">Business Number</label>
									<input type="text" name="fax_area" class="mini" />
									<input type="text" name="fax_number" class="medium" />
							</div>
							<div class="form-labels">
								<p style="padding-top:15px;">Did a rep from Community All-Stars help you? If so, who?</p>
								<input type="text" name="business_rep" class="regular" />
							</div>
							<div class="form-labels">
							<p class="question" style="margin-bottom:0;padding-bottom:0;">Billing Information</p>
							<label class="contact_label" for="first_name">Full Name on Card</label>
								<input type="text" name="bill_first_name" class="medium" />
								<input type="text" name="bill_last_name" class="medium" />
							<label class="contact_label" for="business_address">Street Address</label>
								<input type="text" name="billing_address_1"  class="long" />
							<label class="contact_label" for="billing_city">City</label>
								<input type="text" name="billing_city"  class="long" />
							<label class="contact_label" for="billing_state">State</label>
								<input type="text" name="billing_state"  class="long" />
							<label class="contact_label" for="billing_zip">Zip</label>
								<input type="text" name="billing_zip"  class="long" />
							</div>
							<div style="width:110px;float:left;display:inline-block;margin-top:15px;padding-bottom:5px;">
							<img src="<?php bloginfo("template_directory"); ?>/images/cc-icons.png" width="90"><br>
								<!-- (c) 2005, 2011. Authorize.Net is a registered trademark of CyberSource Corporation --> <div class="AuthorizeNetSeal"> <script type="text/javascript" language="javascript">var ANS_customer_id="69c61a49-061f-442c-8efc-464e1519d914";</script> <script type="text/javascript" language="javascript" src="//verify.authorize.net/anetseal/seal.js" ></script><br></div> 
							</div>
							
							<div style="width:185px;float:left;display:inline-block;">
								<p style="padding-top:10px; margin-bottom:0;">Credit Card Number</p>
									<input type="text" name="cc_card" class="regular" />
								<p style="padding-top:15px; margin-bottom:0;">Expiration Date (MM/YY)</p>
									<input type="text" name="cc_month" class="mini" />
									<input type="text" name="cc_year" class="mini" />
							</div>
								
							<input type="submit" name="submit" class="button pay-submit" value="Submit">
						</div>
					</form>
				</div>
			</div>
			<div class="w-col w-col-3">
				<div class="school-title-box">
					<h3>Pay Invoice</h3>
				</div>
				<div class="school-box">
					Have an invoice you need to pay?<br><Br>
					
					<span id="pay-button" style="text-decoration:underline;cursor:pointer;">Pay it Now</span>
					<form id="pay-form" action="" method="post" style="display:none;">	
					
					<p class="question" style="margin-bottom:0;padding-bottom:0;">Contact Information</p>
					
					<label class="contact_label" for="first_name">First Name</label>
						<input type="text" name="first_name" class="medium" />
					<label class="contact_label" for="first_name">Last Name</label>
						<input type="text" name="last_name" class="medium" />
					<label class="contact_label" for="organization">Organization Name</label>
						<input type="text" name="organization" class="medium" />
					<label class="contact_label" for="phone_area">Contact Phone</label>
						<input type="text" name="phone_area" class="mini" /> <input type="text" name="phone_number" class="medium" />
					<label class="contact_label" for="email">E-Mail</label>
						<input type="text" name="email" class="regular" />
						
					<p class="question" style="margin-bottom:0;padding-bottom:0;">Invoice Information</p>
					
					<label class="contact_label" for="invoice">Invoice #</label>
						<input type="text" name="invoice" class="regular" />
					<label class="contact_label" for="amount">Amount</label>
						<input type="text" name="amount" class="regular" />
					
					
					<p class="question" style="margin-bottom:0;padding-bottom:0;">Billing Information</p>
					
						<label class="contact_label" for="first_name">First Name</label>
							<input type="text" name="bill_first_name" class="medium" />
						<label class="contact_label" for="last_name">Last Name</label>
							<input type="text" name="bill_last_name" class="medium" />
						<label class="contact_label" for="business_address">Street Address</label>
							<input type="text" name="billing_address_1"  class="medium" />
						<label class="contact_label" for="billing_city">City</label>
							<input type="text" name="billing_city"  class="medium" />
						<label class="contact_label" for="billing_state">State</label>
							<input type="text" name="billing_state"  class="medium" />
						<label class="contact_label" for="billing_zip">Zip</label>
							<input type="text" name="billing_zip"  class="medium" />
									
					<p class="question" style="margin-bottom:0;padding-bottom:0;">Credit Card Information</p>
					
					<label class="contact_label" for="cc_card">Credit Card Number</label>
						<input type="text" name="cc_card" class="regular" />
					<label class="contact_label" for="cc_month">Expiration Date MM / YY</label>
						<input type="text" name="cc_month" class="mini" /> / <input type="text" name="cc_year" class="mini" /><br>
						
					<img src="<?php bloginfo("template_directory"); ?>/images/cc-icons.png" width="90">
					<div class="AuthorizeNetSeal"> <script type="text/javascript" language="javascript">var ANS_customer_id="69c61a49-061f-442c-8efc-464e1519d914";</script> <script type="text/javascript" language="javascript" src="//verify.authorize.net/anetseal/seal.js" ></script></div> 


					<input type="submit" name="submit" value="Submit" class="button pay-submit">
					</form>
				</div>
			</div>
		</div>	
	</div>
</div>
<div id="school_modal" class="leanModalDiv">
	<?php if($schools_meta['schools_issuu_code']):?>
		<?php echo $schools_meta['schools_issuu_code'];?>
	<?php else:?>
		<img src="<?php echo $schools_meta['schools_slider_img']; ?>" id="school_flyer">
	<?php endif;?>
</div>
<?php get_footer(); ?>
